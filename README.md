# FogReur: Routing End User Requests to Application Services in Fog Environments

This repository stores all the code, applications, tools, examples, and instructions related to FogReur. Everything found in this repository is a result of the work and research for the _Routing End User Requests to Application Services in Fog Environments_ master's thesis, carried out in Technische Universität Berlin in the Mobile Cloud Computing chair.

The *fogreur* directory includes the source code of all the components that form FogReur - *fog-client*, *fog-controller*, *fog-service*, and *fog-sidecar*.

The *gml2mf* directory includes the source code of the *GML2MF* tool as well as instructions on how to install and use it.

# Running FogReur

We have several options to run and try out FogReur.
* In a local environment:
  * Deploying all the components using Docker
  * Deploy the monitoring components using Docker and FogReur's components directly from an IDE.
* In the cloud using MockFog:
  * Configuring a workspace for [MockFog](https://github.com/MoeweX/MockFog2/tree/v3.0.0) in [Gitpod](https://gitpod.io/).

## Algorithm Selection

As explained in the thesis, 3 load balancing algorithms were implemented to try out FogReur. It's possible to define which one should be used by setting the environment variable `FOGREUR_FRD` to one of the following values:
* `none`
* `clustering`
* `minconflicts`
* `shortestqueues`

If the `FOGREUR_FRD` variable is not set, the components will default to the `none` value and a simple random selection client-side load balancing algorithm will be used.

## Local Deployment

We propose the following scenario for a minimal demonstration deployed locally.

![Local deployment](assets/local-environment-deployment.png)

### Using Docker

The easiest way to deploy FogReur locally is by using Docker. However, deploying all the containers locally may require a considerable amount of resources. In that case, we recommend deploying the containers for the monitoring of distributed traces with Docker and FogReur's components with an IDE.

We need to deploy the following containers:
* Containers to support monitoring of distributed traces:
  * Elasticsearch
  * Kibana (Accessible through http://localhost:5601/)
  * Zipkin (Accessible through http://localhost:9411/)
* FogReur components' containers:
  * Fog Controller (Dashboard at http://localhost:8761/ and detailed information in XML at http://localhost:8761/eureka/apps)
  * 2 Fog Services
  * 2 Fog Sidecar proxies for the Fog Services
  * 1 Fog Client

Ideally, the containers should be run in order.
1. **Elasticsearch**
```bash
docker run -d --restart unless-stopped --name elasticsearch \
    -p 9200:9200 -p 9300:9300 \
    -e "discovery.type=single-node" \
    docker.elastic.co/elasticsearch/elasticsearch:7.9.3
```

2. **Kibana**: The server may take a while to be ready.
```bash
docker run -d --restart unless-stopped --name kibana \
    --link elasticsearch:elasticsearch \
    -p 5601:5601 \
    docker.elastic.co/kibana/kibana:7.9.3
```

3. **Zipkin**
```bash
docker run -d --restart unless-stopped --name zipkin \
    --link elasticsearch \
    -p 9411:9411 \
    -e STORAGE_TYPE=elasticsearch \
    -e ES_HOSTS=http://elasticsearch:9200 \
    openzipkin/zipkin
```

4. **Fog Controller**
```bash
docker run -d --name fog-controller \
    -p 8761:8761 \
    -e FOGREUR_FRD=none \
    -e MACHINE_NAME=fog-controller \
    -e ZIPKIN_IP=localhost \
    fsmatggl/fogreur:fog-controller
```

5. **Fog Services**
```bash
# fog-service-1
docker run -d --name fog-service-1 \
    -p 8081:8081 \
    -e EUREKA_IP=localhost \
    -e MACHINE_NAME=fog-service-1 \
    -e ZIPKIN_IP=localhost \
    -e PORT=8081 \
    fsmatggl/fogreur:fog-service

# fog-service-2
docker run -d --name fog-service-2 \
    -p 8082:8082 \
    -e FOGREUR_FRD=none \
    -e MACHINE_NAME=fog-controller \
    -e ZIPKIN_IP=localhost \
    fsmatggl/fogreur:fog-service
```

6. **Fog Sidecars**
```bash
# fog-sidecar-1
docker run -d --name fog-sidecar-1 \
    -p 5681:5681 \
    -e EUREKA_IP=localhost \
    -e FOGREUR_FRD=none \
    -e HOST_IP=localhost \
    -e MACHINE_NAME=fog-service-sidecar-1 \
    -e SERVICE_NAME=service \
    -e SERVICE_PORT=8081 \
    -e SIDECAR_PORT=5681 \
    -e ZIPKIN_IP=localhost \
    fsmatggl/fogreur:fog-sidecar

# fog-sidecar-2
docker run -d --name fog-sidecar-2 \
    -p 5682:5682 \
    -e EUREKA_IP=localhost \
    -e FOGREUR_FRD=none \
    -e HOST_IP=localhost \
    -e MACHINE_NAME=fog-service-sidecar-2 \
    -e SERVICE_NAME=service \
    -e SERVICE_PORT=8082 \
    -e SIDECAR_PORT=5682 \
    -e ZIPKIN_IP=localhost \
    fsmatggl/fogreur:fog-sidecar
```

7. **Fog Client**
```bash
docker run -d --name fog-client \
    -e EUREKA_IP=localhost \
    -e FOGREUR_FRD=none \
    -e MACHINE_NAME=fog-client \
    -e ZIPKIN_IP=localhost \
    fsmatggl/fogreur:fog-client
```

### Packaging FogReur Applications

FogReur uses Maven to build the applications. Simply use `mvn package` to create a *.jar* file inside the `target` directory.

### Using an IDE

All FogReur applications are Maven applications that can be imported as Maven projects. All applications are SpringBoot applications, which means there's always a `<Component>Application.java` file inside the `ccs.tub.thesis.<component>` package that is executed to run the application, where `<component>` can be either `Client`, `FogController`, `Service`, or `Sidecar`. The only configuration that will be needed is to set the environment variables in the run configuration. These variables are the same that are set when running the applications in containers (See *Using Docker* section).

## Cloud Deployment with MockFog in Gitpod

We have configured a repository that is prepared and configured to run FogReur in an emulated fog network.

Before launching Gitpod, we need to **configure an external server** to run the containers that enable the gathering and visualization of distributed traces. This server requires more resources than the instances that are deployed by MockFog by default. For our experiments we used an EC2 `t2.large` instance. We will need to copy its external IP into the `/workspace/ccs-tub-thesis-mockfog/node-manager/run/config/container.jsonc` file once inside Gitpod or we can use *GML2MF* to take care of it by automatically generating all the configuration files.

**Checklist to set up the monitoring instance in AWS:**
1. Deploy EC2 instance. We recommend to deploy a machine with at least 4GB - at least `t2.medium`.
2. Configure the Security Group of the instance to allow inbound traffic to ports `9411` for Zipkin and `5601` for Kibana.
3. Deploy the following containers according to the *Local Deployment/Using Docker* section.
   1. Elasticsearch
   2. Kibana
   3. Zipkin
4. Copy the external IP address of the instance.
   * If the instance is stopped or restarted, its IP address will change and it will be needed to update it in MockFog's configuration file (*node-manager/run/config/container.jsonc*) or by configuring and using *GML2MF*.

**Accessing Gitpod:**
1. Access the [ccs-tub-thesis-mockfog](https://gitlab.com/dfunke/ccs-tub-thesis-mockfog) repository.
2. Open the repository in Gitpod by pressing the `Gitpod` button.
3. Configure AWS Key and Secret to configure the access to AWS and API: click on the `Gitpod` button on the bottom left and search for the *Gitpod: Open Settings* option. Go to *variables* an add new variables for `AWS_ACCESS_KEY_ID` and`AWS_SECRET_ACCESS_KEY`.
4. Open the `gml2mf/src/config.json` file. There, search for the `zipkin_ip` variable and change `XXX.XXX.XXX.XXX` with the external IP address of your monitoring instance.
5. Generate MockFog's configuration files: open the terminal and type the following.

```bash
cd /workspace/ccs-tub-thesis-mockfog/gml2mf/src
gml2mf
```

MockFog configuration files are directly generated inside the `node-manager/run/config/` directory, where MockFog will look for them to create the emulated fog network.

**Running MockFog** (Additional instructions and details can be found in the [official MockFog documentation](https://github.com/MoeweX/MockFog2/tree/v3.0.0)):
1. Go to the *node-manager* directory

```bash
cd /workspace/ccs-tub-thesis-mockfog/node-manager
```

2. Initialize the infrastructure. This step may take some minutes (3-5 minutes).

```bash
node app.js bootstrap
```

3. Install MockFog's agent in all the hosts. Sometimes the agent is not installed correctly. We recommend to **run the command twice** just in case.

```bash
node app.js agent
```

4. Apply network modifications.

```bash
node app.js manipulate
```

5. Load the container images in the hosts.

```bash
node app.js prepare
```

6. Start the containers.

```bash
node app.js start
```

7. Check Zipkin or Elasticsearch to visualize the data that is generated
   * Visit Zipkin: http://<your_monitoring_instance_address>:9411/zipkin
   * Visit Kibana: http://<your_monitoring_instance_address>:5601/app

8. Stop and destroy the infrastructure

```bash
node app.js stop
node app.js destroy
```

## Zipkin's Elasticsearch Index Limitations and visualization in Kibana

By default, the index that Zipkins creates automatically in Elasticsearch will not configure some attributes properly and it will not be possible to filter, for example, by tags, which can be very interesting.

It is possible to work around this limitation by manually creating the index in Elasticsearch before Zipkin.

If the example setup has been run and Zipkin has received traces and created an index in Elasticsearch, we will have to delete that index first. Go to [Index Management](http://localhost:5601/app/management/data/index_management/indices) and delete the index that Zipkin created. Its name should follow the pattern `zipkin-span-yyyy-mm-dd`.

After it is deleted, we can create a new index with the same name we deleted from the [Development Console](http://localhost:5601/app/dev_tools#/console) by sending the PUT request below.

> Don't forget to change `yyyy-mm-dd` to the current date in the first line.

```json
PUT /zipkin-span-yyyy-mm-dd
{
  "aliases" : { },
  "mappings" : {
    "_source" : {
      "excludes" : [
        "_q"
      ]
    },
    "dynamic_templates" : [
      {
        "strings" : {
          "match" : "*",
          "match_mapping_type" : "string",
          "mapping" : {
            "ignore_above" : 256,
            "norms" : false,
            "type" : "keyword"
          }
        }
      }
    ],
    "properties" : {
      "_q" : {
        "type" : "keyword"
      },
      "annotations" : {
        "type" : "object",
        "enabled" : true
      },
      "duration" : {
        "type" : "long"
      },
      "id" : {
        "type" : "keyword",
        "ignore_above" : 256
      },
      "kind" : {
        "type" : "keyword",
        "ignore_above" : 256
      },
      "localEndpoint" : {
        "dynamic" : "false",
        "properties" : {
          "serviceName" : {
            "type" : "keyword"
          }
        }
      },
      "name" : {
        "type" : "keyword"
      },
      "parentId" : {
        "type" : "keyword",
        "ignore_above" : 256
      },
      "remoteEndpoint" : {
        "dynamic" : "false",
        "properties" : {
          "serviceName" : {
            "type" : "keyword"
          }
        }
      },
      "shared" : {
        "type" : "boolean"
      },
      "tags" : {
        "type" : "object",
        "enabled" : true
      },
      "timestamp" : {
        "type" : "long"
      },
      "timestamp_millis" : {
        "type" : "date",
        "format" : "epoch_millis"
      },
      "traceId" : {
        "type" : "keyword"
      }
    }
  },
  "settings" : {
    "index" : {
      "number_of_shards" : "5",
      "requests" : {
        "cache" : {
          "enable" : "true"
        }
      },
      "number_of_replicas" : "1"
    }
  }
}
```

An index can also be deleted from the console by sending the following request.
```
DELETE /zipkin-span-yyyy-mm-dd
```

Lastly, don't forget to create an index pattern in [Index Patterns](http://localhost:5601/app/management/kibana/indexPatterns/) so Kibana can retrieve the data from the index for its visualization.