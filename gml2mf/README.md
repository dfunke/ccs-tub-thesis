# GML2MF

*GML2MF* is a tool to automatically generate `infrastructure.jsonc` and `deployment.jsonc` configuration files for [MockFog](https://github.com/MoeweX/MockFog2/tree/v3.0.0) based on a graph with *graphml* format.

The tool that has been used to create the graphs is [yEd live](https://www.yworks.com/yed-live/).

## Installation

From the _ccs-tub-thesis/gml2mf_ directory, install the dependencies with NPM.

```bash
npm install
```

Link the application to call it as a command by just typing `gml2mf`.

```bash
npm link
```

If the `extract` key is set to `true` in the configuration file `config.json`, __gml2mf__ will require that `7zip` is installed to uncompress the _.graphmlz_ format, which is the format used by __yEd live__ to save graphs.

```bash
npm install -g 7zip
```

## Use

*GML2MF* is very simple to use. By default, it searches for a `config.json` file in the directory where it is executed. It's possible to specify a different configuration file by using the `--config` option.

```bash
cd src # Go to the directory where the default configuration and example are.

gml2mf # Will run gml2mf a will search for a config.json file in the current directory.

gml2mf --config path/to/config.json # Will run gml2mf using the configuration from the given path.
```

## Configuration

*GML2MF* uses a configuration file to determine which actions it performs. Inside this configuration file we can distinguish between 2 types of options. Those that configure what *GML2MF* does and those that contain values used to generate the MockFog configuration files.

List of variables that configure *GML2MF* behavior:
* `graphz`: if the `extract` option is `true`, is the name of the compressed file that contains the _.gramphml_ file.
* `graphml`: if the `extract` option is `false`, is the name of the _.gramphml_ file that represents the graph.
* `infrastructure_filename`: name of the output MockFog infrastructure configuration file.
* `deployment_filename`: name of the output MockFog deployment configuration file.
* `container_filename`: name of the output MockFog container configuration file.
* `orchestration_filename`: name of the output MockFog orchestration configuration file.
* `extract`: if `true`, will attempt to uncompress the `graphz` file and extract the graph from it.
* `cleanup`: if `true`, will delete all files from the directory after execution, except from the output files
* `coordinates`: if `true`, generates a _.csv_ file that stores each node's X and Y coordinates.

The following values are related to AWS and used to generate and complete MockFog configuration files:
* `ec2_region`
* `ssh_key_name`
* `agent_port`
* `machine_type`
* `machine_image`

The following variables are used to generate the `container.jsonc` file:
* `container_templates`: should contain a template for each different element found in the network graph. Graph nodes' names will match to the template that has the same name without the number. For example, `fog-client-1` will use the template `fog-client`.
* `zipkin_ip`: is the external IP address of the distributed tracing monitoring instance. This value will be set as an environment variable to all the containers despite what's defined in `container_templates`.
* `fogreur_frd`: is the variable that determines which load balancing algorithm configuration to use. Accepted values are `none`, `clustering`, `minconflicts`, or `shortestqueues`. This value will be set as an environment variable to all the containers despite what's defined in `container_templates`.


## Example

A `fog-network.graphml` file has been added to try out *GML2MF*. It represents the following graph:

![Fog Network Example](../assets/fog-network-yedlive.png)

## Creation of FogReur Examples with yEd live

To correctly create the configuration files of a network that will be used to deploy FogReur components in it, it is necessary to specify in those elements that act as services that a sidecar container should be deployed along that services.

To do this, we have to include a simple JSON text inside the *Description* field under the *Custom Data* section in the *PROPERTIES* tab in yEd live.

![Fog Service Sidecar JSON](../assets/yedlive-sidecar.png)