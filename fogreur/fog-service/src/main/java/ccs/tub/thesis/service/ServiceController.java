package ccs.tub.thesis.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ServiceController {
	
	@Value("${server.port}")
	private String port;

    @GetMapping(path = "/process-request")
    public String isAlive() {
        return "service is alive, says instance at port " + port;
    }  
}