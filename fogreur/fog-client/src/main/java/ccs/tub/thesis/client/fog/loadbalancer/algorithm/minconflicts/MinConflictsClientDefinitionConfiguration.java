package ccs.tub.thesis.client.fog.loadbalancer.algorithm.minconflicts;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.client.fog.loadbalancer.FogControllerClient;
import ccs.tub.thesis.client.fog.loadbalancer.FogLoadBalancerDefinition;

@Configuration
public class MinConflictsClientDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "minconflicts")
	public FogLoadBalancerDefinition getFogLoadBalancerDefinition(FogControllerClient fogControllerClient) {
		return new MinConflictsClientDefinition(fogControllerClient);
	}
}
