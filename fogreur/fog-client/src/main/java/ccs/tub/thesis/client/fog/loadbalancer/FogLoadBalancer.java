package ccs.tub.thesis.client.fog.loadbalancer;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.DefaultResponse;
import org.springframework.cloud.client.loadbalancer.EmptyResponse;
import org.springframework.cloud.client.loadbalancer.Request;
import org.springframework.cloud.client.loadbalancer.Response;
import org.springframework.cloud.loadbalancer.core.NoopServiceInstanceListSupplier;
import org.springframework.cloud.loadbalancer.core.ReactorServiceInstanceLoadBalancer;
import org.springframework.cloud.loadbalancer.core.SelectedInstanceCallback;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;

import reactor.core.publisher.Mono;

/**
 */
public class FogLoadBalancer implements ReactorServiceInstanceLoadBalancer {

	private static final Log log = LogFactory.getLog(FogLoadBalancer.class);

	private final String serviceId;

	private ObjectProvider<ServiceInstanceListSupplier> serviceInstanceListSupplierProvider;

	private FogLoadBalancerDefinition loadBalancerDefinition;

	/**
	 * @param serviceInstanceListSupplierProvider a provider of
	 *                                            {@link ServiceInstanceListSupplier}
	 *                                            that will be used to get available
	 *                                            instances
	 * @param serviceId                           id of the service for which to
	 *                                            choose an instance
	 */
	public FogLoadBalancer(ObjectProvider<ServiceInstanceListSupplier> serviceInstanceListSupplierProvider,
			String serviceId, FogLoadBalancerDefinition loadBalancerDefinition) {
		this.serviceId = serviceId;
		this.serviceInstanceListSupplierProvider = serviceInstanceListSupplierProvider;
		this.loadBalancerDefinition = loadBalancerDefinition;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Mono<Response<ServiceInstance>> choose(Request request) {

		ServiceInstanceListSupplier supplier = serviceInstanceListSupplierProvider
				.getIfAvailable(NoopServiceInstanceListSupplier::new);

		return supplier.get(request).next()
				.map(serviceInstances -> processInstanceResponse(supplier, serviceInstances));
	}

	private Response<ServiceInstance> processInstanceResponse(ServiceInstanceListSupplier supplier,
			List<ServiceInstance> serviceInstances) {
		Response<ServiceInstance> serviceInstanceResponse;

		List<ServiceInstance> uniqueInstance = serviceInstances.stream().filter(this::isUnique)
				.collect(Collectors.toList());
		if (!uniqueInstance.isEmpty()) {
			return new DefaultResponse(uniqueInstance.get(0));
		}

		if (serviceInstances.isEmpty()) {
			if (log.isWarnEnabled()) {
				log.warn("No servers available for service: " + serviceId);
			}
			return new EmptyResponse();
		}
		
		if (loadBalancerDefinition == null || loadBalancerDefinition.filterInstances(serviceInstances).isEmpty()) {
			if (log.isWarnEnabled()) {
				log.warn("No servers compatible with definition. Falling back to random selection load balancer.");
			}
			serviceInstanceResponse = getRandomInstanceResponse(serviceInstances);
		} else {
			serviceInstanceResponse = new DefaultResponse(loadBalancerDefinition.choose(serviceInstances));
		}

		if (supplier instanceof SelectedInstanceCallback && serviceInstanceResponse.hasServer()) {
			((SelectedInstanceCallback) supplier).selectedServiceInstance(serviceInstanceResponse.getServer());
		}

		return serviceInstanceResponse;
	}

	private boolean isUnique(ServiceInstance instance) {
		if (instance.getMetadata().containsKey(FogControllerProperties.STATUS.toString())) {
			return instance.getMetadata().get(FogControllerProperties.STATUS.toString())
					.equals(FogControllerProperties.UNIQUE.toString());
		} else {
			return false;
		}
	}

	private Response<ServiceInstance> getRandomInstanceResponse(List<ServiceInstance> instances) {
		int index = ThreadLocalRandom.current().nextInt(instances.size());
		ServiceInstance instance = instances.get(index);
		return new DefaultResponse(instance);
	}
}