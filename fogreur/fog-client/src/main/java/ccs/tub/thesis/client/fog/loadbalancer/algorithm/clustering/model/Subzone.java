package ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering.model;

import java.util.LinkedList;
import java.util.List;

import org.springframework.cloud.client.ServiceInstance;

public class Subzone {
	
	private String name;
	private ServiceInstance prototype;
	private List<Double> latencies = new LinkedList<Double>();
	
	public Subzone(String name, ServiceInstance prototype) {
		super();
		this.name = name;
		this.prototype = prototype;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ServiceInstance getPrototype() {
		return prototype;
	}

	public void setPrototype(ServiceInstance prototype) {
		this.prototype = prototype;
	}

	public List<Double> getLatencies() {
		return latencies;
	}

	public void setLatencies(List<Double> latencies) {
		this.latencies = latencies;
	}
	
	public Double getLatency() {
		return this.latencies.stream().mapToDouble(Double::doubleValue).average().orElse(Double.NaN);
	}

	public void addLatency(Double latency) {
		if (this.latencies.size() >= 5) {
			this.latencies.remove(0);
		}
		this.latencies.add(latency);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("{");
		builder.append("name: " + this.name);
		builder.append(", ");
		if (this.prototype != null) {
			builder.append("prototype: " + this.prototype.getInstanceId());
			builder.append(", ");	
		}
		builder.append("latency: " + this.getLatency());
		builder.append("}");
		
		return builder.toString();
		
	}

}
