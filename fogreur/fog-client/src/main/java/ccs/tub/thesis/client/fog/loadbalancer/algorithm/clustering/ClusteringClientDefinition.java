package ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.cloud.client.ServiceInstance;

import ccs.tub.thesis.client.fog.loadbalancer.FogLoadBalancerDefinition;
import ccs.tub.thesis.client.fog.loadbalancer.FogSidecarClient;

public class ClusteringClientDefinition implements FogLoadBalancerDefinition {

	private ClusteringClientService clusteringClientService;

	public ClusteringClientDefinition(FogSidecarClient fogSidecarClient) {
		this.clusteringClientService = new ClusteringClientService(fogSidecarClient);
	}

	@Override
	public ServiceInstance choose(List<ServiceInstance> instances) {
		ServiceInstance instance;		
		
		instance = clusteringClientService.getSubzonesInstance(instances);

		return instance;
	}
	
	@Override
	public List<ServiceInstance> filterInstances(List<ServiceInstance> serviceInstances) {
		return serviceInstances.stream().filter(this::hasMetadata).collect(Collectors.toList());
	}

	private boolean hasMetadata(ServiceInstance instance) {
		Map<String, String> metadata = instance.getMetadata();
		return metadata.get(ClusteringProperties.CLUSTERING.toString()) != null
				&& metadata.get(ClusteringProperties.PROTOTYPE.toString()) != null
				&& metadata.get(ClusteringProperties.SUBZONE.toString()) != null;
	}
}
