package ccs.tub.thesis.client.fog.loadbalancer;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.loadbalancer.core.ReactorLoadBalancer;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.discovery.EurekaClient;

@Configuration
public class FogLoadBalancerConfiguration {

	@Bean("fog-webclient-builder")
	public WebClient.Builder getWebClientBuilder() {
		return WebClient.builder();
	}

	@Bean
	public ReactorLoadBalancer<ServiceInstance> getReactiveLoadBalancer(Environment environment,
			LoadBalancerClientFactory loadBalancerClientFactory, FogLoadBalancerDefinition fogLoadBalancerDefinition) {
		String name = environment.getProperty("loadbalancer.client.name");
		return new FogLoadBalancer(loadBalancerClientFactory.getLazyProvider(name, ServiceInstanceListSupplier.class),
				name, fogLoadBalancerDefinition);
	}

	@Bean
	public FogControllerClient getFogControllerClient(
			@Qualifier("fog-webclient-builder") WebClient.Builder webClientBuilder, EurekaClient eurekaClient) {
		return new FogControllerClient(webClientBuilder, eurekaClient);
	}
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", matchIfMissing = true, havingValue = "none")
	public FogLoadBalancerDefinition getFogLoadBalancerDefinition() {
		return new EmptyClientDefinition();
	}
}
