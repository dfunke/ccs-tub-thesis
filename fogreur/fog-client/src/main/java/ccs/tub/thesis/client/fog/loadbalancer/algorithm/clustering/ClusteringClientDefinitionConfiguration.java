package ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.client.fog.loadbalancer.FogLoadBalancerDefinition;
import ccs.tub.thesis.client.fog.loadbalancer.FogSidecarClient;

@Configuration
public class ClusteringClientDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "clustering")
	public FogLoadBalancerDefinition getFogLoadBalancerDefinition(FogSidecarClient fogSidecarClient) {
		return new ClusteringClientDefinition(fogSidecarClient);
	}
}