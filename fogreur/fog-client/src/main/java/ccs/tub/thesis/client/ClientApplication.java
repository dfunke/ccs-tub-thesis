package ccs.tub.thesis.client;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import ccs.tub.thesis.client.fog.loadbalancer.FogLoadBalancerConfiguration;

@SpringBootApplication
@EnableEurekaClient
@LoadBalancerClient(value = "service", configuration = FogLoadBalancerConfiguration.class)
public class ClientApplication implements CommandLineRunner {
	
	private ClientRunner clientRunner;
	
	public ClientApplication(ClientRunner clientRunner) {
		this.clientRunner = clientRunner;
	}

	public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		this.clientRunner.run();
		System.exit(0);
	}
}