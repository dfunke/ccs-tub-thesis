package ccs.tub.thesis.client.fog.loadbalancer;

import java.util.List;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

@Component
public interface FogLoadBalancerDefinition {

	ServiceInstance choose(List<ServiceInstance> instances);
	List<ServiceInstance> filterInstances(List<ServiceInstance> serviceInstances);
}
