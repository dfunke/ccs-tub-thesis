package ccs.tub.thesis.client.fog.loadbalancer;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@Component
public class FogControllerClient {

	private static Logger log = LoggerFactory.getLogger(FogControllerClient.class);

	@Value("${eureka.client.service-url.defaultZone}")
	private String eurekaServer;

	private WebClient.Builder webClientBuilder;
	private WebClient webClient;
	private EurekaClient eurekaClient;

	public FogControllerClient(@Qualifier("fog-webclient-builder") WebClient.Builder webClientBuilder,
			EurekaClient eurekaClient) {
		this.webClientBuilder = webClientBuilder;
		this.eurekaClient = eurekaClient;
	}
	
	@PostConstruct
	private void buildWebClient() {
		
		this.webClient = webClientBuilder.baseUrl(eurekaServer.replace("/eureka", "/fog")).build();
	}

	public ServiceInstance getThisInstance() {
		String instanceId = eurekaClient.getApplicationInfoManager().getInfo().getInstanceId();
		String application = eurekaClient.getApplicationInfoManager().getInfo().getAppName();

		return getServiceInstance(application, instanceId);
	}

	public ServiceInstance getServiceInstance(String application, String instanceId) {
		for (Object item : eurekaClient.getInstancesById(instanceId)) {
			InstanceInfo instanceInfo = null;
			if (item instanceof InstanceInfo) {
				instanceInfo = (InstanceInfo) item;
			}
			if (instanceInfo != null && instanceInfo.getAppName().equals(application)) {
				return new EurekaServiceInstance(instanceInfo);
			}
		}

		return null;
	}

	public void triggerOperation(FogOperationRequest request) {
		webClient.post().uri("/trigger-operation").bodyValue(request).retrieve().toBodilessEntity()
				.subscribe(re -> log.info("trigger operation response http status: " + re.getStatusCodeValue()));
	}
}
