package ccs.tub.thesis.client.fog.loadbalancer.algorithm.minconflicts;

public enum MinConflictsOperation {
	INCREMENT_ASSIGNMENT,
	DECREASE_ASSIGNMENT;
}