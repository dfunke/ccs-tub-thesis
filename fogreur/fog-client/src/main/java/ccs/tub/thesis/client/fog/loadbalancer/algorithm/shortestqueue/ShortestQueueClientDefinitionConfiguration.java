package ccs.tub.thesis.client.fog.loadbalancer.algorithm.shortestqueue;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.client.fog.loadbalancer.FogLoadBalancerDefinition;

@Configuration
public class ShortestQueueClientDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "shortestqueues")
	public FogLoadBalancerDefinition getFogLoadBalancerDefinition() {
		return new ShortestQueueClientDefinition();
	}
}
