package ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering.model;

import java.util.LinkedList;
import java.util.List;

import org.springframework.cloud.client.ServiceInstance;

public class Instance {
	
	private String name;
	private ServiceInstance instance;
	private List<Double> latencies = new LinkedList<Double>();
	
	public Instance(ServiceInstance instance) {
		super();
		this.name = instance.getInstanceId();
		this.instance = instance;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ServiceInstance getInstance() {
		return instance;
	}

	public void setInstance(ServiceInstance instance) {
		this.instance = instance;
	}

	public List<Double> getLatencies() {
		return latencies;
	}

	public void setLatencies(List<Double> latencies) {
		this.latencies = latencies;
	}
	
	public Double getLatency() {
		return this.latencies.stream().mapToDouble(Double::doubleValue).average().orElse(Double.NaN);
	}

	public void addLatency(Double latency) {
		if (this.latencies.size() >= 5) {
			this.latencies.remove(0);
		}
		this.latencies.add(latency);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("{");
		builder.append("name: " + this.name);
		builder.append(", ");
		builder.append("latency: " + this.getLatency());
		builder.append("}");
		
		return builder.toString();
		
	}

}
