package ccs.tub.thesis.client.fog.loadbalancer;

public class FogOperationRequest {
	private String application; 
	private String instanceId;
	private String operation;
	private String body;
	
	public FogOperationRequest() {
		super();
	}
	
	public FogOperationRequest(String application, String instanceId, String operation) {
		super();
		this.application = application;
		this.instanceId = instanceId;
		this.operation = operation;
		this.body = "";
	}
	
	public FogOperationRequest(String application, String instanceId, String operation, String body) {
		super();
		this.application = application;
		this.instanceId = instanceId;
		this.operation = operation;
		this.body = body;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}