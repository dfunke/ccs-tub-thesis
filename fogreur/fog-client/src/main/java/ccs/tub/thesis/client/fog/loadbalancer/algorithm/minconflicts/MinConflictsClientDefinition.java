package ccs.tub.thesis.client.fog.loadbalancer.algorithm.minconflicts;

import java.util.List;

import org.springframework.cloud.client.ServiceInstance;

import ccs.tub.thesis.client.fog.loadbalancer.FogControllerClient;
import ccs.tub.thesis.client.fog.loadbalancer.FogLoadBalancerDefinition;
import ccs.tub.thesis.client.fog.loadbalancer.FogOperationRequest;

public class MinConflictsClientDefinition implements FogLoadBalancerDefinition {

	FogControllerClient fogControllerClient;

	public MinConflictsClientDefinition(FogControllerClient fogControllerClient) {
		this.fogControllerClient = fogControllerClient;
	}

	@Override
	public ServiceInstance choose(List<ServiceInstance> instances) {
		ServiceInstance instance;

		instance = instances.stream().reduce((a, b) -> getAssignments(a) < getAssignments(b) ? a : b).get();

		incrementAssignments(instance);

		return instance;
	}

	private Integer getAssignments(ServiceInstance instance) {		
		String value = instance.getMetadata().get(MinConflictsProperties.ASSIGNMENTS.toString());
		if (value == null) {
			value = "0";
			instance.getMetadata().put(MinConflictsProperties.ASSIGNMENTS.toString(), value);
		}
		return Integer.valueOf(value);
	}

	private void incrementAssignments(ServiceInstance instance) {
		FogOperationRequest request = new FogOperationRequest(instance.getServiceId(), instance.getInstanceId(),
				MinConflictsOperation.INCREMENT_ASSIGNMENT.toString());
		fogControllerClient.triggerOperation(request);
	}

	@Override
	public List<ServiceInstance> filterInstances(List<ServiceInstance> serviceInstances) {
		return serviceInstances;
	}
}
