package ccs.tub.thesis.client;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class ClientRunner {

	private static Logger log = LoggerFactory.getLogger(ClientRunner.class);

	@Autowired
	RestTemplate restTemplate;

	public ClientRunner() {
	}

	public void run() throws InterruptedException {

		double startTime;
		double elapsedTime;

		while (true) {

			ResponseEntity<String> response;

			try {
				startTime = System.nanoTime();

				response = this.restTemplate.getForEntity("http://service/service/process-request", String.class);

				elapsedTime = System.nanoTime() - startTime;
				elapsedTime = elapsedTime / 1000000;

				log.info("Response: " + response.getBody() + ". Total elapsed http request/response time: "
						+ elapsedTime + " ms.");

			} catch (HttpClientErrorException | IllegalStateException  e) {
				log.warn(e.getMessage());
			}

			Thread.sleep(new Random().nextInt(10) * 1000);

		}
	}

// Examples how to do some calls
//	private void examples() {
//		ResponseEntity<String> response;
//		response = this.restTemplate.getForEntity("http://localhost:8090/alive", String.class);
//		
//		String baseUrl = client.choose("service").getUri().toString();
//		log.info("Base URL: " + baseUrl);
//		response = this.restTemplate.getForEntity(baseUrl + "/alive", String.class);
//		
//		log.info("Service says: " + response.getBody());
//	}

//  Get Instances from eureka
//	public List<ServiceInstance> serviceInstancesByApplicationName(String applicationName) {
//		log.info("Listing service instances");
//		for (ServiceInstance si : this.discoveryClient.getInstances("service")) {
//			log.info(si.getUri().toString());
//		}
//		
//		return this.discoveryClient.getInstances(applicationName);
//	}

}
