package ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;

import ccs.tub.thesis.client.fog.loadbalancer.FogOperationRequest;
import ccs.tub.thesis.client.fog.loadbalancer.FogSidecarClient;
import ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering.model.Subzone;

public class ClusteringClientService {

	private static Logger log = LoggerFactory.getLogger(ClusteringClientService.class);

	private FogSidecarClient fogSidecarClient;

	private static final String SUBZONE_KEY = ClusteringProperties.SUBZONE.toString();
	private static final String PROTOTYPE_KEY = ClusteringProperties.PROTOTYPE.toString();
	private static final String CLUSTERING_KEY = ClusteringProperties.CLUSTERING.toString();

	private static final Double SWITCH_OVER_THRESHOLD_MULTIPLIER = 1.2d;

	private Subzone subzone = null;
	private List<Subzone> subzones = new LinkedList<Subzone>();

	private int prototypesPosition = 0;
	private List<ServiceInstance> prototypes;

	private String clusteringVersion = "";

	public ClusteringClientService(FogSidecarClient fogSidecarClient) {
		this.fogSidecarClient = fogSidecarClient;
	};

	private ServiceInstance getRandomInstance(List<ServiceInstance> instances) {
		int index = ThreadLocalRandom.current().nextInt(instances.size());
		ServiceInstance instance = instances.get(index);
		return instance;
	}

	public ServiceInstance getSubzonesInstance(List<ServiceInstance> instances) {
		ServiceInstance instance;

		String newestClusteringVersion = instances.get(0).getMetadata().get(CLUSTERING_KEY);
		if (!clusteringVersion.equals(newestClusteringVersion)) {
			log.info("Clustering ID missmatch. Communicating with the prototypes to calculate proximity to sub-zones.");

			prototypes = getPrototypes(instances);
			instance = prototypes.get(++prototypesPosition % prototypes.size());

			updateSubzones(prototypes);
			clusteringVersion = newestClusteringVersion;
		} else {
			log.info("Currently routing requests to sub-zone " + subzone.getName() + ". Current latency is "
					+ subzone.getLatency());

			List<ServiceInstance> subzoneInstances = getSubzoneInstances(instances);
			instance = getRandomInstance(subzoneInstances);
			trySwitchOverSubzone(instance, instances);
		}

		return instance;
	}

	private static List<ServiceInstance> getPrototypes(List<ServiceInstance> instances) {
		return instances.parallelStream().filter(instance -> {
			return "true".equals(instance.getMetadata().get(PROTOTYPE_KEY));
		}).collect(Collectors.toList());
	}

	private List<ServiceInstance> getSubzoneInstances(List<ServiceInstance> instances) {
		return instances.parallelStream().filter(instance -> {
			return subzone.getName().equals(instance.getMetadata().get(SUBZONE_KEY));
		}).collect(Collectors.toList());
	}

	private synchronized void exploreSubzone(ServiceInstance serviceInstance) {
		double latency = 0d;
		latency = fogSidecarClient.timeOperation(new FogOperationRequest(serviceInstance.getServiceId(), serviceInstance.getInstanceId(),
				ClusteringOperation.NOOP.toString()), serviceInstance.getUri().toString());

		Subzone subz = new Subzone(serviceInstance.getMetadata().get(SUBZONE_KEY), serviceInstance);
		subz.addLatency(latency);

		subzones.add(subz);

		log.info("Subzone " + subz.getName() + " explored. Latency " + subz.getLatency());
	}

	private synchronized void prioritizeSubzones() {
		if (subzones.size() <= 1) {
			log.info("There's only one or less sub-zone registered. No priorization will be carried out.");
			log.info(subzones.toString());
		} else {
			subzones.sort((s1, s2) -> s1.getLatency().compareTo(s2.getLatency()));
			log.info("Sub-zones' priorization result: ");
			log.info(subzones.toString());
		}
	}

	private synchronized void updateSubzones(List<ServiceInstance> prototypes) {
		subzones = new LinkedList<Subzone>();

		Runnable r = () -> {
			prototypes.parallelStream().forEach(prototype -> {
				exploreSubzone(prototype);
			});

			prioritizeSubzones();
			subzone = subzones.get(0);
		};

		Thread thread = new Thread(r);
		thread.start();
	}

	private synchronized void trySwitchOverSubzone(ServiceInstance instance, List<ServiceInstance> instances) {
		if (subzones.size() <= 1) {
			log.info("There are no more than one sub-zone. No switch over will be attempted.");
			return;
		}

		double latency;
		latency = fogSidecarClient.timeOperation(new FogOperationRequest(instance.getServiceId(), instance.getInstanceId(),
				ClusteringOperation.NOOP.toString()), instance.getUri().toString());

		subzone.addLatency(latency);

		Subzone switchOverSubzone = subzones.get(1);
		latency = fogSidecarClient.timeOperation(new FogOperationRequest(instance.getServiceId(), instance.getInstanceId(),
				ClusteringOperation.NOOP.toString()), switchOverSubzone.getPrototype().getUri().toString());
		switchOverSubzone.addLatency(latency);

		if (subzone.getLatency() > switchOverSubzone.getLatency() * SWITCH_OVER_THRESHOLD_MULTIPLIER) {
			log.info("Sub-zone's performance degradation " + "(" + subzone.getName() + ":" + subzone.getLatency()
					+ " vs " + switchOverSubzone.getName() + ":" + switchOverSubzone.getLatency() + "). "
					+ "Switching over to " + switchOverSubzone.getName());

			subzones.add(0, switchOverSubzone);
			subzones.add(1, subzone);
			subzone = switchOverSubzone;

			prototypes = getPrototypes(instances);
			updateSubzones(prototypes);
		}
	}
}