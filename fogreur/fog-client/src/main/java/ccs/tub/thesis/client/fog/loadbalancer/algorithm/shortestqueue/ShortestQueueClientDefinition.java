package ccs.tub.thesis.client.fog.loadbalancer.algorithm.shortestqueue;

import java.util.List;

import org.springframework.cloud.client.ServiceInstance;

import ccs.tub.thesis.client.fog.loadbalancer.FogLoadBalancerDefinition;

public class ShortestQueueClientDefinition implements FogLoadBalancerDefinition {

	public ShortestQueueClientDefinition() {
		
	}

	@Override
	public ServiceInstance choose(List<ServiceInstance> instances) {
		return instances.get(0);
	}

	@Override
	public List<ServiceInstance> filterInstances(List<ServiceInstance> serviceInstances) {
		return serviceInstances;
	}
}
