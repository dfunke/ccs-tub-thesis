package ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering;

public enum ClusteringProperties {
	SUBZONE,
	PROTOTYPE,
	CLUSTERING;
}
