package ccs.tub.thesis.client.fog.loadbalancer.algorithm.clustering;

public enum ClusteringOperation {
	NOOP,
	STORE_LATENCIES;
}