package ccs.tub.thesis.client.fog.loadbalancer;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.cloud.client.ServiceInstance;

public class EmptyClientDefinition implements FogLoadBalancerDefinition {
	
	public EmptyClientDefinition() {
		
	}

	@Override
	public ServiceInstance choose(List<ServiceInstance> instances) {
		return getRandomInstanceResponse(instances);
	}
	
	private ServiceInstance getRandomInstanceResponse(List<ServiceInstance> instances) {
		int index = ThreadLocalRandom.current().nextInt(instances.size());
		ServiceInstance instance = instances.get(index);
		return instance;
	}

	@Override
	public List<ServiceInstance> filterInstances(List<ServiceInstance> serviceInstances) {
		return serviceInstances;
	}
}
