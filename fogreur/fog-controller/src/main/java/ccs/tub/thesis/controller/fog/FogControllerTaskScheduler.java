package ccs.tub.thesis.controller.fog;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class FogControllerTaskScheduler {

	ScheduledExecutorService scheduler;
	FogControllerDefinition fogControllerDefinition;

	public FogControllerTaskScheduler(ScheduledExecutorService scheduler, FogControllerDefinition fogControllerDefinition) {
		this.scheduler = scheduler;
		this.fogControllerDefinition = fogControllerDefinition;
	}

	@PostConstruct
	public void addTasksToScheduler() {
		fogControllerDefinition.addIntervalTasks(this.scheduler);
	}
}
