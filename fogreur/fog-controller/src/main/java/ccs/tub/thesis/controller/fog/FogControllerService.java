package ccs.tub.thesis.controller.fog;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.shared.Application;
import com.netflix.eureka.registry.PeerAwareInstanceRegistry;

@Service
public class FogControllerService {

	@Autowired
	PeerAwareInstanceRegistry registry;

	public InstanceInfo getInstanceInfo(String application, String instanceId) {
		for (Object item : registry.getInstancesById(instanceId)) {
			InstanceInfo instanceInfo = null;
			if (item instanceof InstanceInfo) {
				instanceInfo = (InstanceInfo) item;
			}
			if (instanceInfo != null && instanceInfo.getAppName().equals(application)) {
				return instanceInfo;
			}
		}

		return null;
	}

	public InstanceInfo getFirstInstanceInfo(String instanceId) {
		return registry.getInstancesById(instanceId).get(0);
	}

	public void updateMetadata(InstanceInfo instanceInfo, String key, String value) {
		Map<String, String> metadataMap = instanceInfo.getMetadata();
		metadataMap.put(key, value);
		registry.register(instanceInfo, false);
	}

	public void removeMetadata(InstanceInfo instanceInfo, String key) {
		Map<String, String> metadataMap = instanceInfo.getMetadata();
		if (metadataMap.containsKey(key)) {
			metadataMap.remove(key);
			registry.register(instanceInfo, false);
		}
	}

	public void register(InstanceInfo instanceInfo) {
		registry.register(instanceInfo, false);
	}

	public List<InstanceInfo> getAllInstances() {
		List<InstanceInfo> instances = new LinkedList<>();
		List<Application> apps = registry.getApplications().getRegisteredApplications();
		for (Application app : apps) {
			instances.addAll(app.getInstances());
		}
		return instances;
	}

	public void setUnique(InstanceInfo instanceInfo) {
		updateMetadata(instanceInfo, FogControllerProperties.STATUS.toString(), FogControllerProperties.UNIQUE.toString());
	}

	public void removeUnique(InstanceInfo instanceInfo) {
		removeMetadata(instanceInfo, FogControllerProperties.STATUS.toString());
	}
}