package ccs.tub.thesis.controller.fog.algorithm.clustering.model;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.netflix.appinfo.InstanceInfo;

public class ServiceInstanceRecord {
	
	private String instanceId;
	private ServiceInstance serviceInstance;
	private List<Double> latencies = new LinkedList<Double>();
	private Double distance;
	
	public ServiceInstanceRecord() {}
	
	public ServiceInstanceRecord(InstanceInfo instanceInfo) {
		this(new EurekaServiceInstance(instanceInfo));
	}

	public ServiceInstanceRecord(ServiceInstance serviceInstance) {
		super();
		
		if (serviceInstance == null) {
			throw new NullArgumentException("serviceInstance");
		}
		this.serviceInstance = serviceInstance;
		
		if (serviceInstance.getInstanceId() == null) {
			throw new RuntimeException("Trying to create a ServiceInstanceRecord with a ServiceInstance whose instanceId is null.");
		}	
		this.instanceId = serviceInstance.getInstanceId();
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public ServiceInstance getServiceInstance() {
		return serviceInstance;
	}

	public void setServiceInstance(ServiceInstance serviceInstance) {
		this.serviceInstance = serviceInstance;
	}	

	public List<Double> getLatencies() {
		return latencies;
	}

	public void setLatencies(List<Double> latencies) {
		this.latencies = latencies;
	}
	
	public Double getLatency() {
		return this.latencies.stream().mapToDouble(Double::doubleValue).average().orElse(Double.NaN);
	}

	public void addLatency(Double latency) {
		if (this.latencies.size() >= 5) {
			this.latencies.remove(0);
		}
		this.latencies.add(latency);
	}
	
	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		String string = "{instanceId: " + this.instanceId + ", latency: " + this.getLatency() + " {";
		
		int i;
		for (i = 0; i < this.latencies.size() - 1; i++ ) {
			string += this.latencies.get(i) + ", ";
		}
		
		if (!latencies.isEmpty()) {
			string += this.latencies.get(i);
		}
		string += "}}";
		
		return string;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		ServiceInstanceRecord other = (ServiceInstanceRecord) obj;
		return this.instanceId.equals(other.getInstanceId());
	}

	public static Map<String, ServiceInstanceRecord> mapFromJson(String neighborsLatenciesJson) {		
		Gson gson = new GsonBuilder().registerTypeAdapter(ServiceInstance.class, new ServiceInstanceDeserializer()).create();
		Type type = new TypeToken<HashMap<String, ServiceInstanceRecord>>(){}.getType();
		return gson.fromJson(neighborsLatenciesJson, type);
	}
}
