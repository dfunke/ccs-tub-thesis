package ccs.tub.thesis.controller.fog.algorithm.clustering.model;

import java.util.LinkedList;
import java.util.List;

public class Subzone {
	
	private String clusterName;
	private String instancePrototype;
	private List<String> instances = new LinkedList<String>();
	
	public Subzone(String clusterName, String subzoneInstancePrototype, List<String> instances) {
		super();
		this.clusterName = clusterName;
		this.instancePrototype = subzoneInstancePrototype;
		this.instances = instances;
	}
	
	public String getClusterName() {
		return clusterName;
	}
	
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	
	public String getInstancePrototype() {
		return instancePrototype;
	}
	
	public void setInstancePrototype(String instancePrototype) {
		this.instancePrototype = instancePrototype;
	}
	
	public List<String> getInstances() {
		return instances;
	}
	
	public void setInstances(List<String> instances) {
		this.instances = instances;
	}	

}
