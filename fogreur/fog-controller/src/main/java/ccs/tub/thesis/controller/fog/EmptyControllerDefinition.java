package ccs.tub.thesis.controller.fog;

import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

public class EmptyControllerDefinition implements FogControllerDefinition {
	
	public EmptyControllerDefinition() {
		
	}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {
		
	}

	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {
		
	}
}
