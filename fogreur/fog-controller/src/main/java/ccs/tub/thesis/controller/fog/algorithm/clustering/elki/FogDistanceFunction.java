package ccs.tub.thesis.controller.fog.algorithm.clustering.elki;

import de.lmu.ifi.dbs.elki.data.NumberVector;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.distance.distancefunction.NumberVectorDistanceFunction;

public class FogDistanceFunction implements NumberVectorDistanceFunction<NumberVector> {
	
	public FogDistanceFunction() {}

	@Override
	public double distance(NumberVector v1, NumberVector v2) {
		boolean found = false;
		
		for (int i = 0; i < v1.getDimensionality(); i++) {
			for (int j = 0; j < v2.getDimensionality(); j++) {
				if (v1.doubleValue(i) == v2.doubleValue(j)) {
					found = true;
					return v1.doubleValue(i);
				}
			}
		}
		
		if (!found)
			throw new IllegalStateException("Missing the pair of equal distances between 2 instances.");
		
		return 0d;
	}

	@Override
	public SimpleTypeInformation<? super NumberVector> getInputTypeRestriction() {
		return NumberVector.VARIABLE_LENGTH;
	}
}