package ccs.tub.thesis.controller.fog.algorithm.clustering.model;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.appinfo.InstanceInfo.PortType;

public class ServiceInstanceDeserializer implements JsonDeserializer<ServiceInstance> {
	
	@Override
    public ServiceInstance deserialize(JsonElement jElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jObject = jElement.getAsJsonObject();
        jObject = jObject.get("instance").getAsJsonObject();
        
        JsonObject jMetadata = jObject.get("metadata").getAsJsonObject();
        Map<String, String> metadata = new HashMap<String, String>();
        metadata.put("management.port", jMetadata.get("management.port").getAsString());
        
        URI homePage = URI.create(jObject.get("homePageUrl").getAsString());
        URI statusPage = URI.create(jObject.get("statusPageUrl").getAsString());
        URI healthPage = URI.create(jObject.get("healthCheckUrl").getAsString());
        
        
        return new EurekaServiceInstance(InstanceInfo.Builder.newBuilder()
        	.setInstanceId(jObject.get("instanceId").getAsString())
        	.setHostName(jObject.get("hostName").getAsString())
        	.setAppName(jObject.get("appName").getAsString())
        	.setIPAddr(jObject.get("ipAddr").getAsString())
        	.setPort(jObject.get("port").getAsInt())
        	.setSecurePort(jObject.get("securePort").getAsInt())
        	.enablePort(PortType.SECURE, jObject.get("isSecurePortEnabled").getAsBoolean())
        	.enablePort(PortType.UNSECURE, jObject.get("isUnsecurePortEnabled").getAsBoolean())
        	.setHomePageUrl(homePage.getPath(), null)
        	.setStatusPageUrl(statusPage.getPath(), null)
        	.setHealthCheckUrls(healthPage.getPath(), null, null)
        	.setVIPAddress(jObject.get("vipAddress").getAsString())
        	.setSecureVIPAddress(jObject.get("secureVipAddress").getAsString())
        	.setIsCoordinatingDiscoveryServer(jObject.get("isCoordinatingDiscoveryServer").getAsBoolean())
        	.setMetadata(metadata)
        	.build());
    }

}
