package ccs.tub.thesis.controller.fog.algorithm.clustering;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.appinfo.InstanceInfo;

import ccs.tub.thesis.controller.fog.FogControllerService;
import ccs.tub.thesis.controller.fog.algorithm.clustering.model.ServiceInstanceRecord;
import ccs.tub.thesis.controller.fog.algorithm.clustering.model.Subzone;

public class ClusteringControllerService {

	private static Logger log = LoggerFactory.getLogger(ClusteringControllerService.class);
	
	private static final String SUBZONE_KEY = ClusteringProperties.SUBZONE.toString();
	private static final String PROTOTYPE_KEY = ClusteringProperties.PROTOTYPE.toString();
	private static final String CLUSTERING_KEY = ClusteringProperties.CLUSTERING.toString();
		
	private LatenciesStorage latenciesStorage;
	
	FogControllerService fogControllerService;
	
	public ClusteringControllerService(FogControllerService fogControllerService) {
		this.fogControllerService = fogControllerService;
		this.latenciesStorage = new LatenciesStorage(this);
	}

	public void storeLatencies(String instance, String latenciesJson) {
		
		Map<String, ServiceInstanceRecord> latencies = ServiceInstanceRecord.mapFromJson(latenciesJson);
		
		latenciesStorage.store(instance, latencies);		
	}
	
	public void updateMetadata(Map<String, Subzone> currentSubzones) {
		InstanceInfo instanceInfo;
		for (Entry<String, Subzone> e: currentSubzones.entrySet()) {
			String subzone = e.getValue().getClusterName();
			
			for (String instance: e.getValue().getInstances()) {
				try {
					instanceInfo = fogControllerService.getFirstInstanceInfo(instance);
				} catch (Exception ex) {
					log.error(ex.getMessage());
					continue;
				}
				Map<String, String> metadataMap = instanceInfo.getMetadata();
				
				if (instance.equals(e.getValue().getInstancePrototype())) {
					metadataMap.put(PROTOTYPE_KEY, "true");
				} else {
					metadataMap.put(PROTOTYPE_KEY, "false");
				}
				metadataMap.put(SUBZONE_KEY, subzone);
				metadataMap.put(CLUSTERING_KEY, String.valueOf(currentSubzones.hashCode()));
				
				fogControllerService.register(instanceInfo);
			}
		}
	}
}