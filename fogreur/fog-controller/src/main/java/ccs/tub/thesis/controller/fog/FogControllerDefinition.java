package ccs.tub.thesis.controller.fog;

import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

public interface FogControllerDefinition {
	void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest);
	void addIntervalTasks(ScheduledExecutorService scheduler);
}
