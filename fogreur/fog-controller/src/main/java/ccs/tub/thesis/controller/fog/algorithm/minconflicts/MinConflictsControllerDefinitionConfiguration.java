package ccs.tub.thesis.controller.fog.algorithm.minconflicts;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.controller.fog.FogControllerDefinition;
import ccs.tub.thesis.controller.fog.FogControllerService;

@Configuration
public class MinConflictsControllerDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "minconflicts")
	public FogControllerDefinition getFogControllerDefinition(FogControllerService fogControllerService) {
		return new MinConflictsControllerDefinition(fogControllerService);
	}
}
