package ccs.tub.thesis.controller.fog.algorithm.clustering.model;

import java.time.Instant;
import java.util.Date;
import java.util.Map;

public class ServiceInstanceLatencies {
	
	private String instanceId;
	private Map<String, ServiceInstanceRecord> latencies;
	private Date age;
	
	public ServiceInstanceLatencies(String instanceId, Map<String, ServiceInstanceRecord> latencies) {
		super();
		this.instanceId = instanceId;
		this.latencies = latencies;
		this.age = Date.from(Instant.now());
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Map<String, ServiceInstanceRecord> getLatencies() {
		return latencies;
	}

	public void setLatencies(Map<String, ServiceInstanceRecord> latencies) {
		this.latencies = latencies;
	}

	public Date getAge() {
		return age;
	}

	public void setAge(Date age) {
		this.age = age;
	}	
}
