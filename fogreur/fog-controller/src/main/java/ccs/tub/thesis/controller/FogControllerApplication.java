package ccs.tub.thesis.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class FogControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FogControllerApplication.class, args);
	}

}
