package ccs.tub.thesis.controller.fog.algorithm.clustering;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.controller.fog.FogControllerDefinition;
import ccs.tub.thesis.controller.fog.FogControllerService;

@Configuration
public class ClusteringControllerDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "clustering")
	public FogControllerDefinition getFogControllerDefinition(FogControllerService fogControllerService) {
		return new ClusteringControllerDefinition(fogControllerService);
	}
}
