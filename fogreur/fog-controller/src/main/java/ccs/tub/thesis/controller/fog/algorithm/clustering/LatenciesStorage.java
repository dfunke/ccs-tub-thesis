package ccs.tub.thesis.controller.fog.algorithm.clustering;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ccs.tub.thesis.controller.fog.algorithm.clustering.elki.MinMaxClustering;
import ccs.tub.thesis.controller.fog.algorithm.clustering.model.ServiceInstanceLatencies;
import ccs.tub.thesis.controller.fog.algorithm.clustering.model.ServiceInstanceRecord;
import ccs.tub.thesis.controller.fog.algorithm.clustering.model.Subzone;

public class LatenciesStorage {
	
	private final Logger log = LoggerFactory.getLogger(LatenciesStorage.class);
	
	private  Map<String, ServiceInstanceLatencies> neighborsLatencies = new HashMap<String, ServiceInstanceLatencies>();
	private  Map<String, ServiceInstanceLatencies> clusteredNeighborsLatencies = new HashMap<String, ServiceInstanceLatencies>();
	private  boolean isStorageComplete = false;
	
	private final  int OLD_CLUSTERING_THRESHOLD_MINUTES = 10;
	private final  int OLD_LATENCIES_THRESHOLD_SECONDS = 90;
	private  Date clusteringAge = null;
	
	private  Map<String, Subzone> currentSubzones = new HashMap<>();
	
	ClusteringControllerService clusteringControllerService;

	public LatenciesStorage(ClusteringControllerService clusteringControllerService) {
		this.clusteringControllerService = clusteringControllerService;
	}

	public  boolean isStorageComplete() {
		return isStorageComplete;
	}

	public  int getOldClusteringThresholdMinutes() {
		return OLD_CLUSTERING_THRESHOLD_MINUTES;
	}

	public  int getOldLatenciesThresholdSeconds() {
		return OLD_LATENCIES_THRESHOLD_SECONDS;
	}

	public  Date getClusteringAge() {
		return clusteringAge;
	}

	public  void store(String instance, Map<String, ServiceInstanceRecord> latencies) {
		log.info("Stored the following latencies for " + instance + ":");
		latencies.values().forEach((neighbor -> log.info(neighbor.toString())));
		
		neighborsLatencies.put(instance, new ServiceInstanceLatencies(instance, latencies));
		
		tryClustering();
	}
	
	private synchronized  void tryClustering() {
		Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies = new HashMap<>(neighborsLatencies);
		
		if (clusteringNeighborsLatencies.entrySet().size() <= 1) {
			return;
		}
		
		cleanUpOldLatencies(clusteringNeighborsLatencies);
		
		if (!isStorageComplete(clusteringNeighborsLatencies)) {
			return;
		}
		
		if (!areThereChanges(clusteringNeighborsLatencies)) {
			log.info("There are no visible changes compared to the last clustering.");
			if (!isStorageOld()) {
				log.info("The current clustering is still recent. No clustering will be carried out yet.");
				return;
			}
			log.info("The current clustering is already " + OLD_CLUSTERING_THRESHOLD_MINUTES + " minutes old.");
		}
		log.info("Proceeding to cluster the fog instances...");
		
		cluster(clusteringNeighborsLatencies);
	}
	
	public  String getCurrentClustering() {
		StringBuilder builder = new StringBuilder();
		
		for (Entry<String, Subzone> e: currentSubzones.entrySet()) {
			builder.append("Cluster: " + e.getKey() + ". Instances: " + e.getValue().getInstances().toString() + ". Prototype: " + e.getValue().getInstancePrototype());
			builder.append("\n");
		}
		
		return builder.toString();
	}

	private  void cluster(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies) {
		
		MinMaxClustering minmax = new MinMaxClustering();
		currentSubzones = minmax.cluster(clusteringNeighborsLatencies);
		
		for (Entry<String, Subzone> e: currentSubzones.entrySet()) {
			log.info("Cluster: " + e.getKey() + ". Instances: " + e.getValue().getInstances().toString() + ". Prototype: " + e.getValue().getInstancePrototype());
		}
		
		clusteredNeighborsLatencies = clusteringNeighborsLatencies;
		clusteringAge = Date.from(Instant.now());
		
		clusteringControllerService.updateMetadata(currentSubzones);
		
	}

	/**
	 * Checks if the storage has received all the necessary information to perform the clustering.
	 * Any instance that has a latency measurement to a destinationInstance should exist in the destinationInstance's latency measurements list. 
	 * @return true if it has all the necessary information to cluster the services.
	 */
	private synchronized  boolean isStorageComplete(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies) {		
		isStorageComplete = true;
		clusteringNeighborsLatencies.entrySet().forEach(sourceInstance -> {
			sourceInstance.getValue().getLatencies().entrySet().forEach(destinationInstance -> {
				if (!neighborsLatencies.containsKey(destinationInstance.getKey()) ||
						!neighborsLatencies.get(destinationInstance.getKey()).getLatencies().containsKey(sourceInstance.getKey())) {
					isStorageComplete = false;
					return;
				}
			});
		});
		
		log.info("The storage is " + (isStorageComplete ? "complete": "incomplete"));
		
		return isStorageComplete;
	}
	
	private synchronized  void cleanUpOldLatencies(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies) {
		Date now = Date.from(Instant.now());
		List<String> oldLatencies = new LinkedList<String>();
		
		for (Entry<String, ServiceInstanceLatencies> instance: clusteringNeighborsLatencies.entrySet()) {
			Date old = Date.from(Instant.ofEpochMilli(instance.getValue().getAge().getTime() + OLD_LATENCIES_THRESHOLD_SECONDS * 1000));
			if (now.after(old)) {
				oldLatencies.add(instance.getKey());
				log.info("No new latencies from " +  instance.getKey() + " for the last " + OLD_LATENCIES_THRESHOLD_SECONDS + " seconds. "
						+ "The instance will be removed from the latencies storage.");
			}
		}
		
		oldLatencies.forEach(instance -> {
			neighborsLatencies.remove(instance);
			clusteringNeighborsLatencies.remove(instance);
		});
	}

	/**
	 * Checks if there are significant changes in the fog network, such as added or removed services.
	 * @return true if there are added or removed services.
	 */
	private synchronized  boolean areThereChanges(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies) {
		boolean areThereChanges = false;
		
		if (clusteringNeighborsLatencies.entrySet().size() != clusteredNeighborsLatencies.entrySet().size()) {
			return true;
		}
		
		for (Entry<String, ServiceInstanceLatencies> sourceInstance: clusteringNeighborsLatencies.entrySet()) {
			if (!clusteredNeighborsLatencies.containsKey(sourceInstance.getKey())) {
				return true;
			}
			
			for (Entry<String, ServiceInstanceRecord> record: clusteringNeighborsLatencies.get(sourceInstance.getKey()).getLatencies().entrySet()) {
				if (!clusteredNeighborsLatencies.get(sourceInstance.getKey()).getLatencies().containsKey(record.getKey())) {
					return true;
				}
			}
		}		
		
		return areThereChanges;
	}

	/**
	 * Checks if the last clustering is too old. 
	 * If it exceeds a certain age (maybe 1 day), the clustering will be considered too old and will be calculated again, even if there are no changes. 
	 * @return true if exceeds the age threshold.
	 */
	private  boolean isStorageOld() {
		Date now = Date.from(Instant.now());
		Date old = Date.from(Instant.ofEpochMilli(clusteringAge.getTime() + OLD_CLUSTERING_THRESHOLD_MINUTES * 60 * 1000));
		return now.after(old);
	}

}
