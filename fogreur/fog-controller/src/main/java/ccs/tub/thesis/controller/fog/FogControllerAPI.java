package ccs.tub.thesis.controller.fog;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FogControllerAPI {
	
	private FogControllerDefinition fogControllerDefinition;
	
	public FogControllerAPI(FogControllerDefinition fogControllerDefinition) {
		this.fogControllerDefinition = fogControllerDefinition;
	}
	
	@PostMapping(path = "/fog/trigger-operation")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void triggerOperation(HttpServletRequest httpRequest, @RequestBody FogOperationRequest operationRequest) {
		fogControllerDefinition.triggerOperation(httpRequest, operationRequest);
    }
}
