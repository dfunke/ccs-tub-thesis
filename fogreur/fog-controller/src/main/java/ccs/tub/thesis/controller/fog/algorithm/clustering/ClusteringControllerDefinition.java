package ccs.tub.thesis.controller.fog.algorithm.clustering;

import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ccs.tub.thesis.controller.fog.FogControllerDefinition;
import ccs.tub.thesis.controller.fog.FogControllerService;
import ccs.tub.thesis.controller.fog.FogOperationRequest;

public class ClusteringControllerDefinition implements FogControllerDefinition {

	private static Logger log = LoggerFactory.getLogger(ClusteringControllerDefinition.class);

	FogControllerService fogControllerService;
	ClusteringControllerService clusteringControllerService;

	public ClusteringControllerDefinition(FogControllerService fogControllerService) {
		super();
		this.fogControllerService = fogControllerService;
		this.clusteringControllerService = new ClusteringControllerService(fogControllerService);
	}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {
		switch (ClusteringOperation.valueOf(operationRequest.getOperation())) {
		case STORE_LATENCIES:
			clusteringControllerService.storeLatencies(operationRequest.getInstanceId(), operationRequest.getBody());
			break;
		default:
			log.error("Unsupported operation request received");
			break;
		}
	}

	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {
		return;		
	}
}
