package ccs.tub.thesis.controller.fog;

public class FogInformationRequest {
	private String application;
	private String instanceId;
	private String property;
	private String body;
	
	public FogInformationRequest() {
		super();
	}

	public FogInformationRequest(String application, String instanceId, String property) {
		super();
		this.application = application;
		this.instanceId = instanceId;
		this.property = property;
	}

	public FogInformationRequest(String application, String instanceId, String property, String body) {
		super();
		this.application = application;
		this.instanceId = instanceId;
		this.property = property;
		this.body = body;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}