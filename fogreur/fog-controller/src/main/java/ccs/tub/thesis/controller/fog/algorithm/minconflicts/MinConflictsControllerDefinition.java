package ccs.tub.thesis.controller.fog.algorithm.minconflicts;

import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.appinfo.InstanceInfo;

import ccs.tub.thesis.controller.fog.FogControllerDefinition;
import ccs.tub.thesis.controller.fog.FogControllerService;
import ccs.tub.thesis.controller.fog.FogOperationRequest;

public class MinConflictsControllerDefinition implements FogControllerDefinition {

	private static Logger log = LoggerFactory.getLogger(MinConflictsControllerDefinition.class);

	private FogControllerService fogControllerService;

	public MinConflictsControllerDefinition(FogControllerService fogControllerService) {
		super();
		this.fogControllerService = fogControllerService;
	}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {		
		String application = operationRequest.getApplication();
		String serviceId = operationRequest.getInstanceId();
		InstanceInfo instanceInfo = fogControllerService.getInstanceInfo(application, serviceId);

		switch (MinConflictsOperation.valueOf(operationRequest.getOperation())) {
		case INCREMENT_ASSIGNMENT:
			incrementAssignments(instanceInfo);
			break;
		case DECREASE_ASSIGNMENT:
			decrementAssignments(instanceInfo);
			break;
		default:
			log.error("Unsupported operation request received");
			break;

		}
	}

	private void incrementAssignments(InstanceInfo instanceInfo) {
		Integer assignments = getAssignments(instanceInfo);

		fogControllerService.updateMetadata(instanceInfo, MinConflictsProperties.ASSIGNMENTS.toString(),
				String.valueOf(assignments + 1));
		log.info("incrementing assignments");
	}

	private void decrementAssignments(InstanceInfo instanceInfo) {
		Integer assignments = getAssignments(instanceInfo);

		fogControllerService.updateMetadata(instanceInfo, MinConflictsProperties.ASSIGNMENTS.toString(),
				String.valueOf(assignments - 1));
		log.info("decrementing assignments");
	}

	private Integer getAssignments(InstanceInfo instanceInfo) {
		String value = instanceInfo.getMetadata().get(MinConflictsProperties.ASSIGNMENTS.toString());
		if (value == null) {
			value = "0";
		}
		return Integer.valueOf(value);
	}

	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {
		return;
	}
}
