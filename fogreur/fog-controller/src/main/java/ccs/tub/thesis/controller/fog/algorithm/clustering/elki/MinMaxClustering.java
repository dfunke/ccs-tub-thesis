package ccs.tub.thesis.controller.fog.algorithm.clustering.elki;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ccs.tub.thesis.controller.fog.algorithm.clustering.model.ServiceInstanceLatencies;
import ccs.tub.thesis.controller.fog.algorithm.clustering.model.Subzone;
import de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.MiniMaxAnderberg;
import de.lmu.ifi.dbs.elki.algorithm.clustering.hierarchical.extraction.CutDendrogramByNumberOfClusters;
import de.lmu.ifi.dbs.elki.data.Cluster;
import de.lmu.ifi.dbs.elki.data.Clustering;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.NumberVector;
import de.lmu.ifi.dbs.elki.data.model.PrototypeDendrogramModel;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.database.Database;
import de.lmu.ifi.dbs.elki.database.StaticArrayDatabase;
import de.lmu.ifi.dbs.elki.database.datastore.ObjectNotFoundException;
import de.lmu.ifi.dbs.elki.database.ids.DBIDIter;
import de.lmu.ifi.dbs.elki.database.relation.Relation;
import de.lmu.ifi.dbs.elki.datasource.DatabaseConnection;
import de.lmu.ifi.dbs.elki.datasource.InputStreamDatabaseConnection;
import de.lmu.ifi.dbs.elki.datasource.bundle.SingleObjectBundle;
import de.lmu.ifi.dbs.elki.datasource.parser.NumberVectorLabelParser;
import de.lmu.ifi.dbs.elki.datasource.parser.Parser;
import de.lmu.ifi.dbs.elki.distance.distancefunction.DistanceFunction;
import de.lmu.ifi.dbs.elki.result.Result;
import de.lmu.ifi.dbs.elki.result.ResultHierarchy;
import de.lmu.ifi.dbs.elki.result.ResultUtil;
import de.lmu.ifi.dbs.elki.result.SettingsResult;
import de.lmu.ifi.dbs.elki.utilities.ClassGenericsUtil;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.Parameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.SerializedParameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.TrackParameters;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.TrackedParameter;

public class MinMaxClustering {
	
	private static final Logger log = LoggerFactory.getLogger(MinMaxClustering.class);
	
	private SerializedParameterization config;
	private TrackParameters track;
	private Parameterizer par;
	private Parser parser;
	
	public MinMaxClustering() {
		List<String> params = new LinkedList<String>();
		config = new SerializedParameterization(params);
		track = new TrackParameters(config);
		par = ClassGenericsUtil.getParameterizer(NumberVectorLabelParser.class);
		parser = Parser.class.cast(((AbstractParameterizer) par).make(config));
	}
	
	public Map<String, Subzone> cluster(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies) {
		
		String input = prepareAlgorithmInput(clusteringNeighborsLatencies);
		Integer nClusters = calculateNumberOfClusters(clusteringNeighborsLatencies);
		
		InputStream inStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		DatabaseConnection dbc = new InputStreamDatabaseConnection(inStream, null, parser);
		Database db = new StaticArrayDatabase(dbc, null);
		db.initialize();
		
		DistanceFunction<NumberVector> distanceFunction = new FogDistanceFunction();
		MiniMaxAnderberg<NumberVector> ahc = new MiniMaxAnderberg<NumberVector>(distanceFunction);
		CutDendrogramByNumberOfClusters cutAlg = new CutDendrogramByNumberOfClusters(ahc, nClusters, false);
		
		Result res = cutAlg.run(db);
		ResultHierarchy hier = db.getHierarchy();
		hier.add(db, res);
		
		Collection<TrackedParameter> settings = track.getAllParameters();
		hier.add(db, new SettingsResult(settings));
		
	    return getClustering(hier, db);
	}

	private Integer calculateNumberOfClusters(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies) {
		Integer nClusters = 0;
		Integer nInstances = clusteringNeighborsLatencies.size();
		
		if (nInstances <= 0) {
		} else if (nInstances < 2) {
			nClusters = 1;
		} else if (nInstances < 8) {
			nClusters = nInstances / 2;
		} else if (nInstances < 9 ) {
			nClusters = Math.round(nInstances / 3.0f);
		} else {
			nClusters = nInstances / 3;
		}
		
		log.info("Will cluster " + nInstances + " instances in " + nClusters + " clusters");
		
//		return nClusters;
		return 2;
	}

	private String prepareAlgorithmInput(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies) {
		String[] neighbors = new String[clusteringNeighborsLatencies.keySet().size()];
		
		mergeLatencies(clusteringNeighborsLatencies, neighbors);
		String latencies = latenciesToString(clusteringNeighborsLatencies, neighbors);
		
		log.info("Clustering the following nodes and distances:");
		log.info(latencies);
		
		return latencies;
		
	}

	private void mergeLatencies(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies, String[] neighbors) {
		clusteringNeighborsLatencies.keySet().toArray(neighbors);
		
		for (int i = 0; i < neighbors.length; i++) {
			for (int j = 0; j < neighbors.length; j++) {
				if (i == j) {
					continue;
				}
				
				Double sourceLatency = clusteringNeighborsLatencies.get(neighbors[i]).getLatencies().get(neighbors[j]).getLatency();
				Double destinationLatency = clusteringNeighborsLatencies.get(neighbors[j]).getLatencies().get(neighbors[i]).getLatency();
				Double distance = (sourceLatency + destinationLatency) / 2;
				clusteringNeighborsLatencies.get(neighbors[i]).getLatencies().get(neighbors[j]).setDistance(distance);
				clusteringNeighborsLatencies.get(neighbors[j]).getLatencies().get(neighbors[i]).setDistance(distance);
			}
		}
	}
	
	private String latenciesToString(Map<String, ServiceInstanceLatencies> clusteringNeighborsLatencies, String[] neighbors) {
		clusteringNeighborsLatencies.keySet().toArray(neighbors);
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("X Y\n");
		for (int i = 0; i < neighbors.length; i++) {
			builder.append(neighbors[i] + " ");
			
			for (int j = 0; j < neighbors.length; j++) {
				if (i == j) {
					builder.append("0");
				} else {
					builder.append(clusteringNeighborsLatencies.get(neighbors[i]).getLatencies().get(neighbors[j]).getDistance());
				}
				
				if (j != neighbors.length - 1) {
					builder.append(" ");
				}
			}
			
			if (i != neighbors.length - 1) {
				builder.append("\n");
			}
			
		}
		
		return builder.toString();
		
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Subzone> getClustering(ResultHierarchy hier, Result r) {		
		Map<String, Subzone> subzones = new HashMap<String, Subzone>();		
		
		Database db = ResultUtil.findDatabase(hier);
		
		List<Relation<?>> ra = new LinkedList<>();
		List<Clustering<PrototypeDendrogramModel>> rc = new LinkedList<>();

		List<Result> results = ResultUtil.filterResults(db.getHierarchy(), r, Result.class);
		for (Result res : results) {
			if (res instanceof Database) {
				continue;
			}
			if (res instanceof Relation) {
				ra.add((Relation<?>) res);
				continue;
			}
			if (res instanceof Clustering) {
				rc.add((Clustering<PrototypeDendrogramModel>) res);
				continue;
			}
		}

		for (Clustering<PrototypeDendrogramModel> c : rc) {
			int i = 1;
			for (Cluster<PrototypeDendrogramModel> clus : c.getAllClusters()) {
				List<String> instances = getClusterInstances(db, (Cluster<PrototypeDendrogramModel>) clus);
				Relation<LabelList> relLabel = db.getRelation(TypeUtil.LABELLIST);
				
				// If cluster size is 1 set that instance as prototype, for the other cases get the actual cluster prototype
				String prototype = instances.get(0);
				try {
					if (instances.size() > 1) {
						prototype = relLabel.get(clus.getModel().getPrototype()).get(0);
					}
				} catch (ObjectNotFoundException e) {}
				
				Subzone subzone = new Subzone("clu_" + i, prototype, instances);
				subzones.put(subzone.getClusterName(), subzone);
				i++;
			}
		}
		
		return subzones;
	}
	
	private List<String> getClusterInstances(Database db, Cluster<PrototypeDendrogramModel> clus) {
		List<String> instances = new LinkedList<String>();
		
		for(DBIDIter iter = clus.getIDs().iter(); iter.valid(); iter.advance()) {
			SingleObjectBundle bundle = db.getBundle(iter);
		    Object obj = bundle.data(bundle.metaLength() - 1);
		    if (obj != null) {
		    	instances.add(bundle.data(bundle.metaLength() - 1).toString());
		    }
	    }
		
		return instances;
	}
}
