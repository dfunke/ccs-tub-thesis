package ccs.tub.thesis.controller.fog.algorithm.shortestqueue;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.controller.fog.FogControllerDefinition;
import ccs.tub.thesis.controller.fog.FogControllerService;
import ccs.tub.thesis.controller.fog.FogSidecarClient;

@Configuration
public class ShortestQueueControllerDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "shortestqueues")
	public FogControllerDefinition getFogControllerDefinition(FogSidecarClient fogSidecarClient, FogControllerService fogControllerService) {
		return new ShortestQueueControllerDefinition(fogSidecarClient, fogControllerService);
	}
}
