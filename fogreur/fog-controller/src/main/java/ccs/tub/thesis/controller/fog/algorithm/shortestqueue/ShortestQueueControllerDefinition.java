package ccs.tub.thesis.controller.fog.algorithm.shortestqueue;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance;

import com.netflix.appinfo.InstanceInfo;

import ccs.tub.thesis.controller.fog.FogControllerDefinition;
import ccs.tub.thesis.controller.fog.FogControllerService;
import ccs.tub.thesis.controller.fog.FogInformationRequest;
import ccs.tub.thesis.controller.fog.FogOperationRequest;
import ccs.tub.thesis.controller.fog.FogSidecarClient;

public class ShortestQueueControllerDefinition implements FogControllerDefinition {

	private static Logger log = LoggerFactory.getLogger(ShortestQueueControllerDefinition.class);

	private Map<String, String> shortestQueues = new HashMap<>();
	private Map<String, Map<String, Integer>> queuesSizes = new HashMap<>();

	private FogSidecarClient fogSidecarClient;
	private FogControllerService fogControllerService;

	public ShortestQueueControllerDefinition(FogSidecarClient fogSidecarClient,
			FogControllerService fogControllerService) {
		super();
		this.fogSidecarClient = fogSidecarClient;
		this.fogControllerService = fogControllerService;
	}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {
		return;
	}

	private void retrieveSidecarsQueuesSizes() {
		FogInformationRequest informationRequest;
		for (InstanceInfo instance : fogControllerService.getAllInstances()) {
			EurekaServiceInstance eInstance = new EurekaServiceInstance(instance);
			informationRequest = new FogInformationRequest(instance.getAppName(), instance.getInstanceId(),
					ShortestQueueProperties.QUEUE_LENGTH.toString());
			fogSidecarClient.getInformation(informationRequest, eInstance.getUri().toString(),
					this::processQueuesSizesResponse);
		}
	}

	private void processQueuesSizesResponse(FogInformationRequest informationRequest) {
		String application = informationRequest.getApplication();
		String instanceId = informationRequest.getInstanceId();
		Integer queueSize = Integer.valueOf(informationRequest.getBody());

		StringBuilder keyBuilder = new StringBuilder();
		keyBuilder.append(application).append(":").append(instanceId);
		log.info("Triggering processQueuesSizesResponse callback. Storing queue size " + queueSize + " for "
				+ keyBuilder.toString());

		Map<String, Integer> appInstances;
		if (!this.queuesSizes.containsKey(application)) {
			appInstances = new HashMap<>();
			this.queuesSizes.put(application, appInstances);
			this.shortestQueues.put(application, instanceId);
		} else {
			appInstances = this.queuesSizes.get(informationRequest.getApplication());
		}
		appInstances.put(informationRequest.getInstanceId(), Integer.valueOf(informationRequest.getBody()));

		updateShortestQueue(application, instanceId, queueSize);
	}

	private void updateShortestQueue(String application, String instanceId, Integer queueSize) {
		String shortestQueueInstanceId = shortestQueues.get(application);
		Integer shortestQueueSize = this.queuesSizes.get(application).get(shortestQueueInstanceId);
		InstanceInfo shortestQueueInstance = fogControllerService.getInstanceInfo(application, shortestQueueInstanceId);

		if (shortestQueueSize >= queueSize) {
			InstanceInfo newShortestQueueInstance = fogControllerService.getInstanceInfo(application, instanceId);

			this.shortestQueues.put(application, instanceId);
			fogControllerService.removeUnique(shortestQueueInstance);
			fogControllerService.setUnique(newShortestQueueInstance);
		}
	}

	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {
		scheduler.scheduleAtFixedRate(this::retrieveSidecarsQueuesSizes, 60, 60, TimeUnit.SECONDS);
	}
}
