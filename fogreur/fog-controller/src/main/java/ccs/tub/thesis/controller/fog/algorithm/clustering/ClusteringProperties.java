package ccs.tub.thesis.controller.fog.algorithm.clustering;

public enum ClusteringProperties {
	SUBZONE,
	PROTOTYPE,
	CLUSTERING;
}
