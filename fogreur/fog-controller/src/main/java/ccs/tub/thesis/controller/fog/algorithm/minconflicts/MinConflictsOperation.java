package ccs.tub.thesis.controller.fog.algorithm.minconflicts;

public enum MinConflictsOperation {
	INCREMENT_ASSIGNMENT,
	DECREASE_ASSIGNMENT;
}