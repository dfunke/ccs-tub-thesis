package ccs.tub.thesis.controller.fog;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class FogControllerConfiguration {
	
	@Bean("fog-webclient-builder")
	public WebClient.Builder getWebClientBuilder() {
		return WebClient.builder();
	}
	
    @Bean
    public ScheduledExecutorService getScheduledExecutorService() {
    	return Executors.newScheduledThreadPool(1);
    }
    
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", matchIfMissing = true, havingValue = "none")
	public FogControllerDefinition getFogLoadBalancerDefinition() {
		return new EmptyControllerDefinition();
	}
}
