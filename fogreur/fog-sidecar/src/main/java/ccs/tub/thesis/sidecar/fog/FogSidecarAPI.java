package ccs.tub.thesis.sidecar.fog;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ccs.tub.thesis.sidecar.fog.algorithm.shortestqueue.ShortestQueueProperties;

@RestController
public class FogSidecarAPI {
	
	private FogSidecarDefinition fogSidecarDefinition;
	
	public FogSidecarAPI(FogSidecarDefinition fogSidecarDefinition) {
		this.fogSidecarDefinition = fogSidecarDefinition;
	}
	
	@PostMapping(path = "/trigger-operation")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void triggerOperation(HttpServletRequest httpRequest, @RequestBody FogOperationRequest operationRequest) {
		fogSidecarDefinition.triggerOperation(httpRequest, operationRequest);
    }
	
	@PostMapping(path = "/information")
    public ResponseEntity<?> getInformation(@RequestBody FogInformationRequest informationRequest) {
		FogInformationRequest response = fogSidecarDefinition.retrieveInformation(informationRequest);
		return ResponseEntity.status(HttpStatus.OK).body(response);
    }
	
	@GetMapping(path = "/information")
    public ResponseEntity<?> getInformation() {
		FogInformationRequest response = new FogInformationRequest("app", "instance", ShortestQueueProperties.QUEUE_LENGTH.toString(), "2");
		return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
