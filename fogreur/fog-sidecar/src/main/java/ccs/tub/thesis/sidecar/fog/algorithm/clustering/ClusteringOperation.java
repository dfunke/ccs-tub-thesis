package ccs.tub.thesis.sidecar.fog.algorithm.clustering;

public enum ClusteringOperation {
	NOOP,
	STORE_LATENCIES;
}