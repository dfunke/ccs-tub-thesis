package ccs.tub.thesis.sidecar.fog;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.context.RequestContext;

public class EmptySidecarDefinition implements FogSidecarDefinition {
	
	public EmptySidecarDefinition() {
		
	}

	@Override
	public List<SupportedRequest> enabledPreRequests() {
		return Collections.emptyList();
	}

	@Override
	public List<SupportedRequest> enabledPostRequests() {
		return Collections.emptyList();
	}

	@Override
	public void preRequest(RequestContext ctx) {}

	@Override
	public void postRequest(RequestContext ctx) {}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {}

	@Override
	public FogInformationRequest retrieveInformation(FogInformationRequest informationRequest) {
		return new FogInformationRequest();
	}

	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {}
}
