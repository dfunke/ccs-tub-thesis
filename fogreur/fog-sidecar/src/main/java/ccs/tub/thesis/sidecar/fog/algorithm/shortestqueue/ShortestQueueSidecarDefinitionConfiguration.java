package ccs.tub.thesis.sidecar.fog.algorithm.shortestqueue;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.sidecar.fog.FogSidecarDefinition;

@Configuration
public class ShortestQueueSidecarDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "shortestqueues")
	public FogSidecarDefinition getFogSidecarDefinition() {
		return new ShortestQueueSidecarDefinition();
	}
}