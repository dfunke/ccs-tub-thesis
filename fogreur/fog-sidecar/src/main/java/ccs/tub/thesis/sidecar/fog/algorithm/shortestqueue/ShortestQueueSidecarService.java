package ccs.tub.thesis.sidecar.fog.algorithm.shortestqueue;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class ShortestQueueSidecarService {
	
	private List<String> taskQueue = new LinkedList<>();

	public ShortestQueueSidecarService() {

	}

	public void addTaskToQueue(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();
		sb.append(request.getMethod()).append(" ");
		sb.append(request.getRequestURI()).append(" ");
		sb.append(request.getProtocol());
		taskQueue.add(sb.toString());		
	}

	public void removeTaskFromQueue(HttpServletRequest request) {
		taskQueue.remove(0);
	}

	public int getQueueLength() {
		return taskQueue.size();
	}
}