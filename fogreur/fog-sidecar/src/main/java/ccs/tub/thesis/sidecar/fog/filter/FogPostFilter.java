package ccs.tub.thesis.sidecar.fog.filter;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import ccs.tub.thesis.sidecar.fog.FogSidecarDefinition;
import ccs.tub.thesis.sidecar.fog.SupportedRequest;

public class FogPostFilter extends ZuulFilter {
	
	private FogSidecarDefinition fogSidecarDefinition;
	
	public FogPostFilter(FogSidecarDefinition fogSidecarDefinition) {
		this.fogSidecarDefinition = fogSidecarDefinition;
	}

	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		
		for (SupportedRequest supportedRequest : fogSidecarDefinition.enabledPostRequests()) {
			if (supportedRequest.getMethod().toString().equals(request.getMethod()) && request.getRequestURI().contains(supportedRequest.getPath())) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();		
		fogSidecarDefinition.postRequest(ctx);
		return null;
	}
}
