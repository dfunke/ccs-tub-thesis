package ccs.tub.thesis.sidecar.fog.algorithm.clustering;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.sidecar.fog.FogControllerClient;
import ccs.tub.thesis.sidecar.fog.FogSidecarClient;
import ccs.tub.thesis.sidecar.fog.FogSidecarDefinition;

@Configuration
public class ClusteringSidecarDefinitionConfiguration {

	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "clustering")
	public FogSidecarDefinition getFogSidecarDefinition(FogControllerClient fogControllerClient,
			FogSidecarClient fogSidecarClient) {
		return new ClusteringSidecarDefinition(fogControllerClient, fogSidecarClient);
	}
}