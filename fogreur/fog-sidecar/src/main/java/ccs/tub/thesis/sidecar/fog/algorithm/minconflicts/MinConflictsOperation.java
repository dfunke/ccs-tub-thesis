package ccs.tub.thesis.sidecar.fog.algorithm.minconflicts;

public enum MinConflictsOperation {
	INCREMENT_ASSIGNMENT,
	DECREASE_ASSIGNMENT;
}