package ccs.tub.thesis.sidecar.fog.algorithm.clustering.model;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.springframework.cloud.client.ServiceInstance;

public class ServiceInstanceRecord {
	
	private String instanceId;
	private ServiceInstance serviceInstance;
	private List<Double> latencies = new LinkedList<Double>();

	public ServiceInstanceRecord(ServiceInstance serviceInstance) {
		super();
		
		if (serviceInstance == null) {
			throw new NullArgumentException("serviceInstance");
		}
		this.serviceInstance = serviceInstance;
		
		if (serviceInstance.getInstanceId() == null) {
			throw new RuntimeException("Trying to create a ServiceInstanceRecord with a ServiceInstance whose instanceId is null.");
		}	
		this.instanceId = serviceInstance.getInstanceId();
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public ServiceInstance getServiceInstance() {
		return serviceInstance;
	}

	public void setServiceInstance(ServiceInstance serviceInstance) {
		this.serviceInstance = serviceInstance;
	}
	
	public List<Double> getLatencies() {
		return latencies;
	}

	public void setLatencies(List<Double> latencies) {
		this.latencies = latencies;
	}
	
	public Double getLatency() {
		return this.latencies.stream().mapToDouble(Double::doubleValue).average().orElse(Double.NaN);
	}

	public void addLatency(Double latency) {
		if (this.latencies.size() >= 5) {
			this.latencies.remove(0);
		}
		this.latencies.add(latency);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		ServiceInstanceRecord other = (ServiceInstanceRecord) obj;
		return this.instanceId.equals(other.getInstanceId());
	}	
}
