package ccs.tub.thesis.sidecar.fog;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.context.RequestContext;

public interface FogSidecarDefinition {
	
	List<SupportedRequest> enabledPreRequests();
	List<SupportedRequest> enabledPostRequests();
	void preRequest(RequestContext ctx);
	void postRequest(RequestContext ctx);
	void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest);
	FogInformationRequest retrieveInformation(FogInformationRequest informationRequest);
	void addIntervalTasks(ScheduledExecutorService scheduler);
}
