package ccs.tub.thesis.sidecar.fog.algorithm.shortestqueue;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import com.netflix.zuul.context.RequestContext;

import ccs.tub.thesis.sidecar.fog.FogInformationRequest;
import ccs.tub.thesis.sidecar.fog.FogOperationRequest;
import ccs.tub.thesis.sidecar.fog.FogSidecarDefinition;
import ccs.tub.thesis.sidecar.fog.SupportedRequest;

public class ShortestQueueSidecarDefinition implements FogSidecarDefinition {
	
	private static Logger log = LoggerFactory.getLogger(ShortestQueueSidecarDefinition.class);
	
	private ShortestQueueSidecarService shortestQueueSidecarService;
	
	public ShortestQueueSidecarDefinition() {
		this.shortestQueueSidecarService = new ShortestQueueSidecarService();
	}
	
	@Override
	public List<SupportedRequest> enabledPreRequests() {
		List<SupportedRequest> supportedRequests = new LinkedList<SupportedRequest>();
		supportedRequests.add(new SupportedRequest(HttpMethod.GET, "/alive"));
		return supportedRequests;
	}

	@Override
	public List<SupportedRequest> enabledPostRequests() {
		List<SupportedRequest> supportedRequests = new LinkedList<SupportedRequest>();
		supportedRequests.add(new SupportedRequest(HttpMethod.GET, "/alive"));
		return supportedRequests;
	}

	@Override
	public void preRequest(RequestContext ctx) {
		HttpServletRequest request = ctx.getRequest();
		shortestQueueSidecarService.addTaskToQueue(request);
	}

	@Override
	public void postRequest(RequestContext ctx) {
		HttpServletRequest request = ctx.getRequest();
		shortestQueueSidecarService.removeTaskFromQueue(request);
	}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {
		
	}

	@Override
	public FogInformationRequest retrieveInformation(FogInformationRequest informationRequest) {
		switch (ShortestQueueProperties.valueOf(informationRequest.getProperty())) {
		case QUEUE_LENGTH:
			int queueLength = shortestQueueSidecarService.getQueueLength();
			informationRequest.setBody(String.valueOf(queueLength));
			return informationRequest;
		default:
			log.error("Unsupported information request received");
			break;
		}
		
		return null;
	}

	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {
		return;
	}
}
