package ccs.tub.thesis.sidecar.fog;

import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class FogSidecarTaskScheduler {

	ScheduledExecutorService scheduler;
	FogSidecarDefinition fogSidecarDefinition;

	public FogSidecarTaskScheduler(ScheduledExecutorService scheduler, FogSidecarDefinition fogSidecarDefinition) {
		this.scheduler = scheduler;
		this.fogSidecarDefinition = fogSidecarDefinition;
	}
	
	@PostConstruct
	public void addTasksToScheduler() {
		fogSidecarDefinition.addIntervalTasks(this.scheduler);
	}
	
	
}
