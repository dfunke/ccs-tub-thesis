package ccs.tub.thesis.sidecar.fog.algorithm.minconflicts;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.http.HttpMethod;

import com.netflix.zuul.context.RequestContext;

import ccs.tub.thesis.sidecar.fog.FogControllerClient;
import ccs.tub.thesis.sidecar.fog.FogInformationRequest;
import ccs.tub.thesis.sidecar.fog.FogOperationRequest;
import ccs.tub.thesis.sidecar.fog.FogSidecarDefinition;
import ccs.tub.thesis.sidecar.fog.SupportedRequest;

public class MinConflictsSidecarDefinition implements FogSidecarDefinition {
	
	private static Logger log = LoggerFactory.getLogger(MinConflictsSidecarDefinition.class);
	
	private FogControllerClient fogControllerClient;
	
	public MinConflictsSidecarDefinition(FogControllerClient fogControllerClient) {
		this.fogControllerClient = fogControllerClient;
	}
	
	@Override
	public List<SupportedRequest> enabledPreRequests() {
		return Collections.emptyList();
	}

	@Override
	public List<SupportedRequest> enabledPostRequests() {
		List<SupportedRequest> supportedRequests = new LinkedList<SupportedRequest>();
		supportedRequests.add(new SupportedRequest(HttpMethod.GET, "/alive"));
		return supportedRequests;
	}

	@Override
	public void preRequest(RequestContext ctx) {
		
	}

	@Override
	public void postRequest(RequestContext ctx) {
		HttpServletRequest request = ctx.getRequest();
		log.info("request uri: " + request.getRequestURI());
		
		ServiceInstance instance = fogControllerClient.getThisInstance();
		reduceAssignments(instance);
	}
	
	private void reduceAssignments(ServiceInstance instance) {
		FogOperationRequest request = new FogOperationRequest(instance.getServiceId(), instance.getInstanceId(),
				MinConflictsOperation.DECREASE_ASSIGNMENT.toString());
		fogControllerClient.triggerOperation(request);
	}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {
		return;
	}

	@Override
	public FogInformationRequest retrieveInformation(FogInformationRequest informationRequest) {
		return null;
	}
	
	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {
		return;
	}
}
