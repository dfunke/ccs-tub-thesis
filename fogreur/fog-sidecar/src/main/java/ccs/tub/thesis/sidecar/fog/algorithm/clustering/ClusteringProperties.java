package ccs.tub.thesis.sidecar.fog.algorithm.clustering;

public enum ClusteringProperties {
	SUBZONE,
	PROTOTYPE,
	CLUSTERING;
}
