package ccs.tub.thesis.sidecar.fog.algorithm.clustering;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.context.RequestContext;

import ccs.tub.thesis.sidecar.fog.FogControllerClient;
import ccs.tub.thesis.sidecar.fog.FogInformationRequest;
import ccs.tub.thesis.sidecar.fog.FogOperationRequest;
import ccs.tub.thesis.sidecar.fog.FogSidecarClient;
import ccs.tub.thesis.sidecar.fog.FogSidecarDefinition;
import ccs.tub.thesis.sidecar.fog.SupportedRequest;

public class ClusteringSidecarDefinition implements FogSidecarDefinition {

	private static Logger log = LoggerFactory.getLogger(ClusteringSidecarDefinition.class);

	private ClusteringSidecarService clusteringSidecarService;

	public ClusteringSidecarDefinition(FogControllerClient fogControllerClient, FogSidecarClient fogSidecarClient) {
		this.clusteringSidecarService = new ClusteringSidecarService(fogControllerClient, fogSidecarClient);
	}

	@Override
	public List<SupportedRequest> enabledPreRequests() {
		return Collections.emptyList();
	}

	@Override
	public List<SupportedRequest> enabledPostRequests() {
		return Collections.emptyList();
	}

	@Override
	public void preRequest(RequestContext ctx) {

	}

	@Override
	public void postRequest(RequestContext ctx) {

	}

	@Override
	public void triggerOperation(HttpServletRequest httpRequest, FogOperationRequest operationRequest) {
		switch (ClusteringOperation.valueOf(operationRequest.getOperation())) {
		case NOOP:
			break;
		default:
			log.error("Unsupported operation request received");
			break;
		}
	}

	@Override
	public void addIntervalTasks(ScheduledExecutorService scheduler) {
		scheduler.scheduleAtFixedRate(clusteringSidecarService::sendLatenciesToRegistry, 60, 60, TimeUnit.SECONDS);
	}

	@Override
	public FogInformationRequest retrieveInformation(FogInformationRequest informationRequest) {
		return null;
	}
}
