package ccs.tub.thesis.sidecar.fog;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

@Component
public class FogSidecarClient {

	private static Logger log = LoggerFactory.getLogger(FogSidecarClient.class);

	private WebClient.Builder webClientBuilder;

	public FogSidecarClient(@Qualifier("fog-webclient-builder") WebClient.Builder webClientBuilder) {
		this.webClientBuilder = webClientBuilder;
	}
	
	private Mono<ResponseEntity<Void>> prepareOperation(FogOperationRequest request, String baseUrl) {
		WebClient webClient = webClientBuilder.baseUrl(baseUrl).build();
		return webClient.post().uri("/trigger-operation").bodyValue(request).retrieve().toBodilessEntity();
	}
	
	private void blockOperation(Mono<ResponseEntity<Void>> operation) {
		operation.block();
	}
	
	public void triggerOperation(FogOperationRequest request, String baseUrl) {
		WebClient webClient = webClientBuilder.baseUrl(baseUrl).build();
		webClient.post().uri("/trigger-operation").bodyValue(request).retrieve().toBodilessEntity()
				.subscribe(re -> log.info("trigger sidecar operation response http status: " + re.getStatusCodeValue()));
	}
	
	public double timeOperation(FogOperationRequest request, String baseUrl) {
		Mono<ResponseEntity<Void>> operation = prepareOperation(request, baseUrl);

		double startTime;
		double elapsedTime;
		startTime = System.nanoTime();

		blockOperation(operation);

		elapsedTime = System.nanoTime() - startTime;
		elapsedTime = elapsedTime / 1000000;

		return elapsedTime;
	}
	
	public void getInformation(FogInformationRequest request, String baseUrl, Consumer<? super FogInformationRequest> callback) {
		WebClient webClient = webClientBuilder.baseUrl(baseUrl).build();
		webClient.post().uri("/fog/information").bodyValue(request).retrieve().bodyToMono(FogInformationRequest.class).subscribe(callback);
	}
}
