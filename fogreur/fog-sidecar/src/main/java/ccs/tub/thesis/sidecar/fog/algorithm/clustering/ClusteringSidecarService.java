package ccs.tub.thesis.sidecar.fog.algorithm.clustering;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;

import com.google.gson.Gson;

import ccs.tub.thesis.sidecar.fog.FogControllerClient;
import ccs.tub.thesis.sidecar.fog.FogOperationRequest;
import ccs.tub.thesis.sidecar.fog.FogSidecarClient;
import ccs.tub.thesis.sidecar.fog.algorithm.clustering.model.ServiceInstanceRecord;

public class ClusteringSidecarService {

	private static Logger log = LoggerFactory.getLogger(ClusteringSidecarService.class);

	private Map<String, ServiceInstanceRecord> neighborsLatencies = new HashMap<String, ServiceInstanceRecord>();

	FogControllerClient fogControllerClient;
	FogSidecarClient fogSidecarClient;

	public ClusteringSidecarService(FogControllerClient fogControllerClient, FogSidecarClient fogSidecarClient) {
		this.fogControllerClient = fogControllerClient;
		this.fogSidecarClient = fogSidecarClient;
	};

	public void sendLatenciesToRegistry() {
		log.info("AAAAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHH sendLatenciesToRegistry");
		updateLatencies();
		if (!neighborsLatencies.entrySet().isEmpty()) {
			sendToRegistry(neighborsLatencies);
		}
	}

	private void updateLatencies() {
		Map<String, ServiceInstanceRecord> newLatencies = new HashMap<String, ServiceInstanceRecord>();

		List<ServiceInstance> serviceInstances = fogControllerClient
				.getApplicationInstances(fogControllerClient.getThisInstanceApplication());

		if (serviceInstances.isEmpty()) {
			log.info("No additional services to calculate latencies were found.");
		}

		serviceInstances.parallelStream().forEach(serviceInstance -> {
			double latency = 0d;
			try {
				latency = fogSidecarClient.timeOperation(new FogOperationRequest(serviceInstance.getServiceId(),
						serviceInstance.getInstanceId(), ClusteringOperation.NOOP.toString()),
						serviceInstance.getUri().toString());
			} catch (Exception e) {
				log.warn("Exception when trying to ping " + serviceInstance.getHost() + ":" + serviceInstance.getPort()
						+ ".");
				log.warn(e.getMessage());
				return;
			}

			ServiceInstanceRecord instanceRecord;
			if (neighborsLatencies.containsKey(serviceInstance.getInstanceId())) {
				instanceRecord = neighborsLatencies.get(serviceInstance.getInstanceId());
			} else {
				instanceRecord = new ServiceInstanceRecord(serviceInstance);
			}
			instanceRecord.addLatency(latency);

			newLatencies.put(instanceRecord.getInstanceId(), instanceRecord);
			neighborsLatencies = newLatencies;
		});
	}

	private void sendToRegistry(Map<String, ServiceInstanceRecord> neighborsLatencies) {

		log.info("Sending the latencies to the registry.");

		Gson gson = new Gson();
		String body = gson.toJson(neighborsLatencies);

		String thisApplication = fogControllerClient.getThisInstanceApplication();
		String thisInstanceId = fogControllerClient.getThisInstanceId();

		FogOperationRequest request = new FogOperationRequest(thisApplication, thisInstanceId,
				ClusteringOperation.STORE_LATENCIES.toString(), body);
		fogControllerClient.triggerOperation(request);
	}
}