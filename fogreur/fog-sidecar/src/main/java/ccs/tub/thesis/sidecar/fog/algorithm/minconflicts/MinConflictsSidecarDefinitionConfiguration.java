package ccs.tub.thesis.sidecar.fog.algorithm.minconflicts;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ccs.tub.thesis.sidecar.fog.FogControllerClient;
import ccs.tub.thesis.sidecar.fog.FogSidecarDefinition;

@Configuration
public class MinConflictsSidecarDefinitionConfiguration {
	
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", havingValue = "minconflicts")
	public FogSidecarDefinition getFogSidecarDefinition(FogControllerClient fogControllerClient) {
		return new MinConflictsSidecarDefinition(fogControllerClient);
	}
}