package ccs.tub.thesis.sidecar.fog;

import org.springframework.http.HttpMethod;

public class SupportedRequest {
	
	private HttpMethod method;
	private String path;
	
	public SupportedRequest(HttpMethod method, String path) {
		super();
		this.method = method;
		this.path = path;
	}
	public HttpMethod getMethod() {
		return method;
	}
	public void setMethod(HttpMethod method) {
		this.method = method;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	

}
