package ccs.tub.thesis.sidecar.fog;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.discovery.EurekaClient;

import ccs.tub.thesis.sidecar.fog.filter.FogPostFilter;
import ccs.tub.thesis.sidecar.fog.filter.FogPreFilter;

@Configuration
public class FogSidecarConfiguration {
	
	@Bean("fog-webclient-builder")
	public WebClient.Builder getWebClientBuilder() {
		return WebClient.builder();
	}
	
	@Bean
	public FogControllerClient getFogControllerClient(WebClient.Builder webClientBuilder, EurekaClient eurekaClient) {
		return new FogControllerClient(webClientBuilder, eurekaClient);
	}

	@Bean
	public FogPreFilter getFogPreFilter(FogSidecarDefinition fogSidecarDefinition) {
		return new FogPreFilter(fogSidecarDefinition);
	}
	
	@Bean
	public FogPostFilter getFogPostFilter(FogSidecarDefinition fogSidecarDefinition) {
		return new FogPostFilter(fogSidecarDefinition);
	}
	
    @Bean
    public ScheduledExecutorService getScheduledExecutorService() {
    	return Executors.newScheduledThreadPool(1);
    }
    
	@Bean
	@ConditionalOnProperty(name = "fogreur.frd", matchIfMissing = true, havingValue = "none")
	public FogSidecarDefinition getFogLoadBalancerDefinition() {
		return new EmptySidecarDefinition();
	}
}
